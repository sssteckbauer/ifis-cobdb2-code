       IDENTIFICATION DIVISION.
       PROGRAM-ID.    SSNFIX2.
      ******************************************************************
      *    TITLE ......... SSN CLEAN UP1 - CLEAN EMPID
      *    LANGUAGE ...... COBOL II / BATCH / DB2
      *
      *
      *   READ PRS TABLE AND IF  EMP-ID FOUND IN ENTITY, DELETE PPI PCH
      *   AND BLANK OUT THE EMP-ID.
      *
      ******************************************************************

      ******************************************************************
      *                      MAINTENANCE HISTORY                       *
      ******************************************************************
      *    MODIFIED BY .....                                           *
      *    DATE MODIFIED ...                                           *
      *    MODIFICATION ....                                           *
      ******************************************************************
      /
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT RPT1-FILE            ASSIGN RPT1.
           SELECT CRPT-FILE            ASSIGN CNTLRPT.

       DATA DIVISION.
       FILE SECTION.

       FD  RPT1-FILE
           RECORDING MODE F
           BLOCK CONTAINS 0 RECORDS
           LABEL RECORDS OMITTED.
       01  RPT1-RECORD                 PIC X(132).

       FD  CRPT-FILE
           RECORDING MODE F
           BLOCK CONTAINS 0 RECORDS
           LABEL RECORDS OMITTED.
       01  CRPT-RECORD                 PIC X(132).
      /
       WORKING-STORAGE SECTION.
      ******************************************************************
      *    MISCELLANEOUS WORK AREAS
      ******************************************************************
       01  FILLER                      PIC X(36)       VALUE
           'SSNFIX2 WORKING-STORAGE BEGINS HERE '.

       01  WS-CONSTANTS.
           05  WS-THIS-PROGRAM         PIC X(08)       VALUE 'SSNFIX2 '.
           05  WS-COMMIT-POINT         PIC S9(7)       COMP-3
                                                       VALUE +100.
           05  WS-HIGH-DATE            PIC X(10)       VALUE
                                       '2999-12-31'.
           05  WS-HIGH-DATE-TS         PIC X(26)       VALUE
                                       '2999-12-31-23.59.59.999999'.
           05  WS-LOW-DATE-TS          PIC X(26)       VALUE
                                       '1800-01-01-00.00.00.000001'.

       01  WS-ABEND-AREA.
           05  WS-ABEND-PARAGRAPH      PIC X(30)       VALUE SPACES.
           05  WS-ABEND-SQL-NBR        PIC S9(4)       VALUE ZEROES.
           05  WS-ABEND-CODE           PIC -999        VALUE ZEROES.

       01  WS-DATE-AREA.
           05  WS-SYS-DATE.
               10  WS-SYS-DATE-CC      PIC 9(02)       VALUE ZEROES.
               10  WS-SYS-DATE-YYMMDD.
                   15  WS-SYS-DATE-YY  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-MM  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-DD  PIC 9(02)       VALUE ZEROES.
           05  WS-SYSTEM-DATE          REDEFINES WS-SYS-DATE
                                       PIC 9(08).
           05  WS-RPT-DATE.
               10  WS-RPT-DATE-MM      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-DD      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-YY      PIC X(02)       VALUE SPACES.

       01  WS-WORK-AREA.
           05  WS-EMPID-FOUND          PIC S9(8) COMP  VALUE ZEROES.
           05  WS-COMMIT-COUNT         PIC S9(7)       COMP-3
                                                       VALUE ZEROES.
           05  WS-REPORT-PERSON-SW     PIC X(01)       VALUE 'N'.
           05  NO-MORE-PRS-SW          PIC X(01)       VALUE 'N'.
           05  EMP-ID-FOUND-ENT-SW     PIC X(01)       VALUE 'N'.

           05  WS-MESSAGE.
               10  WS-MESSAGE-PRS      PIC X(16)       VALUE SPACES.
               10  WS-MESSAGE-SPLIT    PIC X(03)       VALUE SPACES.
               10  WS-MESSAGE-PRA      PIC X(16)       VALUE SPACES.
      /
      ******************************************************************
      *    PARM INPUT RECORD AREA
      ******************************************************************
       01  PARM-COUNT                  PIC 9(08)       VALUE ZEROES.
       01  PARM-AREA.
           05  PARM-PROGRAM            PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  PARM-BYP-NEWID          PIC X(13)       VALUE SPACES.
           05  FILLER                  PIC X(58)       VALUE SPACES.

      ******************************************************************
      *    CID INPUT RECORD AREA (CAMPUS INTEGRATED DIRECTORY)
      ******************************************************************
       01  CID-COUNT                   PIC 9(08)       VALUE ZEROES.
      /
      ******************************************************************
      *    RPT1 REPORT AREA
      ******************************************************************
       01  RPT1-COUNTERS.
           05  RPT1-PAGE-COUNT         PIC 9(03)       VALUE ZEROES.
           05  RPT1-LINE-COUNT         PIC 9(03)       VALUE 99.
           05  RPT1-DETAIL-COUNT       PIC 9(08)       VALUE ZEROES.

       01  RPT1-HEADING-1.
           05  FILLER                  PIC X(08)       VALUE 'SSNFIX2 '.
           05  FILLER                  PIC X(40)       VALUE SPACES.
           05  FILLER                  PIC X(35)       VALUE
               'UNIVERSITY OF CALIFORNIA, SAN DIEGO'.
           05  FILLER                  PIC X(40)       VALUE SPACES.
           05  FILLER                  PIC X(05)       VALUE 'PAGE '.
           05  RPT1-PAGE               PIC Z(04)       VALUE ZEROES.

       01  RPT1-HEADING-2.
           05  FILLER                  PIC X(56)       VALUE SPACES.
           05  FILLER                  PIC X(20)       VALUE
               '    IFIS SYSTEM     '.
           05  FILLER                  PIC X(56)       VALUE SPACES.

       01  RPT1-HEADING-3.
           05  FILLER                  PIC X(44)       VALUE SPACES.
           05  FILLER                  PIC X(43)       VALUE
               'CAMPUS INTEGRATED DIRECTORY UPDATES TO IFIS'.
           05  FILLER                  PIC X(25)       VALUE SPACES.
           05  FILLER                  PIC X(12)       VALUE
                                                       'REPORT DATE '.
           05  RPT1-RUN-DATE           PIC X(08)       VALUE SPACES.

       01  RPT1-HEADING-4.
           05  FILLER                  PIC X(112)      VALUE SPACES.
           05  FILLER                  PIC X(12)       VALUE
                                                       'REPORT TIME '.
           05  RPT1-RUN-TIME           PIC X(08)       VALUE SPACES.

       01  RPT1-DTL-HDG-1.
           05  FILLER                  PIC X(09)       VALUE '   SSN  '.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(09)       VALUE '   PID  '.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(09)       VALUE 'EMP ID  '.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  FILLER                  PIC X(04)       VALUE 'STAT'.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(08)       VALUE 'USER ID '.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(20)       VALUE
                                                  '   EMAIL ADDRESS   '.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(26)       VALUE
                                                  '       NAME        '.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  FILLER                  PIC X(35)       VALUE
                                  '              MESSAGE              '.

       01  RPT1-DTL-HDG-2.
           05  FILLER                  PIC X(09)       VALUE ALL '-'.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(09)       VALUE ALL '-'.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(09)       VALUE ALL '-'.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  FILLER                  PIC X(04)       VALUE ALL '-'.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(08)       VALUE ALL '-'.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(20)       VALUE ALL '-'.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  FILLER                  PIC X(26)       VALUE ALL '-'.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  FILLER                  PIC X(35)       VALUE ALL '-'.

       01  RPT1-DTL-LINE-1.
           05  RPT1-SSN                PIC X(09)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  RPT1-PID                PIC ZZZZZZZZ9   VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  RPT1-EMP-ID             PIC X(09)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  RPT1-EMP-STATUS         PIC X(01)       VALUE SPACES.
           05  FILLER                  PIC X(04)       VALUE SPACES.
           05  RPT1-USER-ID            PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  RPT1-EMAIL-ADDR         PIC X(20)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  RPT1-NAME-KEY           PIC X(26)       VALUE SPACES.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  RPT1-MESSAGE            PIC X(35)       VALUE SPACES.

       01  RPT1-BLANK-LINE.
           05  FILLER                  PIC X(01)       VALUE SPACES.

       01  RPT1-NO-DTLS.
           05  FILLER                  PIC X(32)       VALUE
               'NO RECORDS FOUND FOR THIS REPORT'.

       01  RPT1-END-OF-LIST.
           05  FILLER                  PIC X(11)       VALUE
               'END OF LIST'.
      /
      ******************************************************************
      *    CONTROL REPORT AREA
      ******************************************************************
       01  DATE-CONV-COB2.             COPY WSDATEC2.

       01  TIME-CONV-COB2.             COPY WSTIMEC2.

       01  WHEN-COMP-CONV.             COPY WHENCMC2.

DEVMJM* START
           COPY SYSRGHTC.
DEVMJM* END

       01  CRPT-COUNTERS.
           05  CRPT-PAGE-COUNT         PIC 9(03)       VALUE ZEROES.
           05  CRPT-LINE-COUNT         PIC 9(03)       VALUE 99.

       01  CRPT-HEADING-1.
           05  FILLER                  PIC X(08)       VALUE 'SSNFIX2 '.
           05  FILLER                  PIC X(40)       VALUE SPACES.
           05  FILLER                  PIC X(35)       VALUE
               'UNIVERSITY OF CALIFORNIA, SAN DIEGO'.
           05  FILLER                  PIC X(35)       VALUE SPACES.
           05  CRPT-PAGE               PIC Z(04)       VALUE ZEROES.
           05  FILLER                  PIC X(10)       VALUE SPACES.

       01  CRPT-HEADING-2.
           05  FILLER                  PIC X(07)       VALUE 'RUN ON'.
           05  CRPT-RUN-DATE           PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(04)       VALUE ' AT'.
           05  CRPT-RUN-TIME.
               10  CRPT-RUN-TIME-HH    PIC 9(02)       VALUE ZEROES.
               10  FILLER              PIC X(01)       VALUE ':'.
               10  CRPT-RUN-TIME-MM    PIC 9(02)       VALUE ZEROES.
               10  FILLER              PIC X(01)       VALUE ':'.
               10  CRPT-RUN-TIME-SS    PIC 9(02)       VALUE ZEROES.
           05  FILLER                  PIC X(17)       VALUE SPACES.
           05  FILLER                  PIC X(78)       VALUE
               'CAMPUS INTEGRATED DIRECTORY UPDATES TO IFIS'.

       01  CRPT-HEADING-3.
           05  FILLER                  PIC X(59)       VALUE SPACES.
           05  FILLER                  PIC X(14)       VALUE
               'CONTROL REPORT'.
           05  FILLER                  PIC X(59)       VALUE SPACES.

       01  CRPT-DTL-LINE-1             PIC X(132)      VALUE SPACES.
      /
      ******************************************************************
      *    DB2 ERROR HANDLING WORK AREA
      ******************************************************************

           EXEC SQL INCLUDE DB2ERRBW
           END-EXEC.
      /
      ******************************************************************
      *    SQL AREA AND TABLE DB2 TABLE DCLGENS
      ******************************************************************

           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
       01  PPI-PRSN-PRVID-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'PPI*'.
           05  PPI-EXISTS-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PPI-INSERT-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PPI-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPPI
           END-EXEC.
      /
       01  PRS-PRSN-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'PRS*'.
           05  PRS-SELECT-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRS-BYPASS-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRS-READ-COUNT          PIC 9(08)       VALUE ZEROES.
           05  PRS-UNCHNG-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PCH-DELETE-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PPI-DELETE-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRS-UPDATE-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRS-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPRS
           END-EXEC.
      /
       01  PCH-PRSN-ADR-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'PRA*'.
           05  PCH-SELECT-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PCH-BYPASS-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PCH-UNCHNG-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PCH-UPDATE-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PCH-INSERT-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PCH-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPCH
           END-EXEC.

      /
      ******************************************************************
      *    PARM AREA USED BY SUBROUTINE UPEU035X TO RECEIVE AN INPUT   *
      *    ID AND THE BASIC INFORMATION NEEDED TO ADD THE DATABASE     *
      *    ROWS NEEDED TO ESTABLISH A NEW PERSON ON IFIS.              *
      ******************************************************************

      /
           EXEC SQL
               DECLARE PRS_CURSOR  CURSOR WITH HOLD FOR
               SELECT
                  IREF_ID
                 ,SSN_DGT_ONE
                 ,SSN_LST_NINE
                 ,NAME_KEY
                 ,EMP_ID
                 ,EMP_STATUS
             FROM PRS_PRSN_V
             END-EXEC.


       PROCEDURE DIVISION.
      /
       A0000-MAINLINE.

      ******************************************************************
      *    PERFORM THE PROGRAM'S MAINLINE FUNCTIONS.
      ******************************************************************

           PERFORM A1000-INITIALIZATION.

           MOVE 'N'   TO NO-MORE-PRS-SW

           PERFORM B1000-PROCESS-PRS-CLEANUP
              THRU B1000-EXIT
               UNTIL NO-MORE-PRS-SW = 'Y'.

           PERFORM A8000-FINALIZATION.

           GOBACK.
      /
       A1000-INITIALIZATION.
      ******************************************************************
      *    OPEN FILES AND INITIALIZE WORKING STORAGE AREAS.
      *    READ THE PARM INPUT FILE PARAMETER RECORD.
      ******************************************************************

           MOVE 'A1000-INITIALIZATION' TO WS-ABEND-PARAGRAPH.

           DISPLAY '>>>>  SSNFIX2 STARTING  <<<<'.

           OPEN
                OUTPUT RPT1-FILE
                       CRPT-FILE.

           MOVE FUNCTION CURRENT-DATE(1:8)
                                       TO WS-SYS-DATE.
           MOVE WS-SYS-DATE-YY         TO WS-RPT-DATE-YY.
           MOVE WS-SYS-DATE-MM         TO WS-RPT-DATE-MM.
           MOVE WS-SYS-DATE-DD         TO WS-RPT-DATE-DD.
           MOVE WS-RPT-DATE            TO CRPT-RUN-DATE.

           COPY PDTIMEC2.

           MOVE TIME-HH                TO CRPT-RUN-TIME-HH.
           MOVE TIME-MM                TO CRPT-RUN-TIME-MM.
           MOVE TIME-SS                TO CRPT-RUN-TIME-SS.

           MOVE WHEN-COMPILED          TO COMPILED-DATA.
           INSPECT COMPILED-DATA-TIME
                   REPLACING ALL '.' BY ':'.
           STRING 'PROGRAM COMPILED '     COMPILED-DATA-DATE
                  ' AT '                  COMPILED-DATA-TIME
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.

           PERFORM A9000-PRINT-CRPT-DETAIL.

           MOVE CRPT-RUN-DATE          TO RPT1-RUN-DATE.
           MOVE CRPT-RUN-TIME          TO RPT1-RUN-TIME.


           EXEC SQL
                OPEN PRS_CURSOR
           END-EXEC.
           IF SQLCODE NOT = +0
               GO TO S9999-DB2-ERROR.

      /
       A8000-FINALIZATION.
      ******************************************************************
      *    REPORT/DISPLAY FINAL TOTALS AND CLOSE FILES.
      ******************************************************************

           MOVE 'A8000-FINALIZATION'   TO WS-ABEND-PARAGRAPH.

           COPY PDTIMEC2.

           MOVE 'PROGRAM STATUS'       TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           MOVE SPACES                 TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           MOVE SPACES                 TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF PRS RECORDS READ             =>  '
                   PRS-READ-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF EMP ID      FOUND ON ENTITY  =>  '
                   PRS-SELECT-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.


           STRING 'NBR OF EMP ID            DELETED    =>  '

                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           MOVE SPACES                 TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.


           MOVE SPACES                 TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.


           STRING 'PROGRAM HAS BEEN SUCCESSFULLY COMPLETED AT '
                   TIME-HH ':' TIME-MM
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           MOVE ALL 'END OF REPORT '   TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           IF RPT1-DETAIL-COUNT > 0
               WRITE RPT1-RECORD     FROM RPT1-END-OF-LIST
                                    AFTER ADVANCING 2 LINES
           ELSE
               PERFORM D1500-PRINT-RPT1-HEADING
               WRITE RPT1-RECORD     FROM RPT1-NO-DTLS
                                    AFTER ADVANCING 2 LINES
           END-IF.

           CLOSE
                 RPT1-FILE
                 CRPT-FILE.

           EXEC SQL
               CLOSE PRS_CURSOR
           END-EXEC.

           IF SQLCODE NOT = +0
               GO TO S9999-DB2-ERROR.

           IF WS-COMMIT-COUNT > 0
               PERFORM S9998-COMMIT-UPDATES
           END-IF.

           DISPLAY 'RECORDS REPORTED  = ' RPT1-DETAIL-COUNT.

           DISPLAY '>>>>  SSNFIX2 FINISHED  <<<<'.
      /
       A9000-PRINT-CRPT-DETAIL.
      ******************************************************************
      *    WRITE CONTROL REPORT DETAIL LINE.
      ******************************************************************

           MOVE 'A9000-PRINT-CRPT-DETAIL' TO WS-ABEND-PARAGRAPH.

           IF CRPT-LINE-COUNT > 57
               PERFORM A9500-PRINT-CRPT-HEADING
           END-IF.

           WRITE CRPT-RECORD         FROM CRPT-DTL-LINE-1
                                    AFTER ADVANCING 2.

           MOVE SPACES                 TO CRPT-DTL-LINE-1.
           ADD  2                      TO CRPT-LINE-COUNT.


       A9500-PRINT-CRPT-HEADING.
      ******************************************************************
      *    WRITE CONTROL REPORT PAGE HEADINGS.
      ******************************************************************

           MOVE 'A9500-PRINT-CRPT-HEADING' TO WS-ABEND-PARAGRAPH.

           ADD  1                      TO CRPT-PAGE-COUNT.
           MOVE CRPT-PAGE-COUNT        TO CRPT-PAGE.

           WRITE CRPT-RECORD         FROM CRPT-HEADING-1
                                    AFTER ADVANCING PAGE.
           WRITE CRPT-RECORD         FROM CRPT-HEADING-2
                                    AFTER ADVANCING 1.
           WRITE CRPT-RECORD         FROM CRPT-HEADING-3
                                    AFTER ADVANCING 1.

           MOVE 3                      TO CRPT-LINE-COUNT.
      /
       B1000-PROCESS-PRS-CLEANUP.
      ******************************************************************
      *    FOR EACH CID RECORD, SELECT THE PRS/PPI PERSON INFORMATION
      *    WHERE THE INPUT SSN# MATCHES SSN-LST-NINE ON THE PRS TABLE.
      *    IF FOUND, PERFORM ROUTINE TO CHECK WHICH TABLES TO UPDATE.
      *    IF NOT FOUND, PERFORM ROUTINE TO ADD A NEW PERSON.
      *    IF MORE THAN 1 ROW IS FOUND FOR THE SSN (SQLCODE= -811),
      *    BYPASS THE UPDATES AND REPORT THE DUPLICATION.
      ******************************************************************

           MOVE 'B1000-PROCESS-PRS-CLEANUP' TO WS-ABEND-PARAGRAPH.
           MOVE  +2000                      TO WS-ABEND-SQL-NBR.

            EXEC SQL
              FETCH PRS_CURSOR
                    INTO
                  :PRS-IREF-ID
                 ,:PRS-SSN-DGT-ONE
                 ,:PRS-SSN-LST-NINE
                 ,:PRS-NAME-KEY
                 ,:PRS-EMP-ID
                 ,:PRS-EMP-STATUS
            END-EXEC.

            IF SQLCODE = +0
              NEXT SENTENCE
            ELSE
               IF SQLCODE = +100
                  MOVE 'Y'    TO NO-MORE-PRS-SW
                  GO TO  B1000-EXIT
               ELSE
                   GO TO S9999-DB2-ERROR.

           MOVE 'N'                    TO WS-REPORT-PERSON-SW
                                          EMP-ID-FOUND-ENT-SW.

           ADD 1 TO PRS-READ-COUNT
           IF PRS-EMP-ID > SPACES
                PERFORM S1000-CHECK-ENI
                   THRU S1000-EXIT
                IF EMP-ID-FOUND-ENT-SW  = 'Y'  AND
                   PRS-EMP-STATUS NOT = 'A'
                   MOVE PRS-EMP-ID  TO RPT1-EMP-ID
                   ADD 1 TO PRS-SELECT-COUNT
                   PERFORM C3000-DELETE-PCH-PPI
                      THRU C3000-EXIT
                   PERFORM U2000-UPDATE-PRS
                      THRU U2000-EXIT
                   ADD 1 TO WS-COMMIT-COUNT
                 END-IF
           END-IF.

           IF WS-REPORT-PERSON-SW = 'Y'
               MOVE PRS-SSN-LST-NINE       TO RPT1-SSN
               MOVE PRS-IREF-ID            TO RPT1-PID
               MOVE PRS-NAME-KEY           TO RPT1-NAME-KEY
               MOVE PRS-EMP-STATUS         TO RPT1-EMP-STATUS
               PERFORM D1000-PRINT-RPT1-DETAIL
           END-IF.

           IF WS-COMMIT-COUNT > WS-COMMIT-POINT
               PERFORM S9998-COMMIT-UPDATES
           END-IF.

      /
       B1000-EXIT.
             EXIT.

       C3000-DELETE-PCH-PPI.

           MOVE 'Y' TO WS-REPORT-PERSON-SW
           MOVE 'C3000-DELETE-PCH-PPI' TO WS-ABEND-PARAGRAPH.
           MOVE  +3000                 TO WS-ABEND-SQL-NBR.
           MOVE ZEROS                  TO SQLCODE.

           EXEC SQL
               SELECT
                  PRSN_ID_DGT_ONE
                 ,PRSN_ID_LST_NINE
                 ,CHANGE_DT
                 ,FK_PRS_IREF_ID
                 ,PRS_SET_TS
                 ,FK_PCH_IREF_ID
                 ,FK_PCH_CHG_DT
                 ,FK_PCH_SET_TS
                 ,PCH_SET_TS
               INTO
                  :PPI-PRSN-ID-DGT-ONE
                 ,:PPI-PRSN-ID-LST-NINE
                 ,:PPI-CHANGE-DT
                 ,:PPI-FK-PRS-IREF-ID
                 ,:PPI-PRS-SET-TS
                 ,:PPI-FK-PCH-IREF-ID
                 ,:PPI-FK-PCH-CHG-DT
                 ,:PPI-FK-PCH-SET-TS
                 ,:PPI-PCH-SET-TS
             FROM PPI_PRSN_PRVID_V
                 WHERE PRSN_ID_DGT_ONE  = ' '
                   AND PRSN_ID_LST_NINE = :PRS-EMP-ID
             END-EXEC.

             IF SQLCODE = +100
                 DISPLAY 'EMP-ID NOT FOUND IN PPI:' PRS-EMP-ID
                 GO TO C3000-EXIT
              ELSE
                 IF SQLCODE = +0
                     ADD 1 TO PPI-DELETE-COUNT
                 ELSE
                     GO TO S9999-DB2-ERROR.

******** DELETE PCH FOR FORST
           MOVE  +3500                 TO WS-ABEND-SQL-NBR.
           MOVE ZEROS                  TO SQLCODE.
             EXEC SQL
                  DELETE FROM PCH_PRSN_CHG_V
               WHERE FK_PRS_IREF_ID = :PPI-FK-PCH-IREF-ID
                AND  CHANGE_DT      = :PPI-FK-PCH-CHG-DT
                AND  PRS_SET_TS     = :PPI-FK-PCH-SET-TS
             END-EXEC.

           ADD 1 TO PCH-DELETE-COUNT

******** DELETE PPI
           MOVE  +3600                 TO WS-ABEND-SQL-NBR.
           MOVE ZEROS                  TO SQLCODE.

             EXEC SQL
                 DELETE FROM PPI_PRSN_PRVID_V
                     WHERE  PRSN_ID_DGT_ONE  = ' '
                     AND    PRSN_ID_LST_NINE = :PRS-EMP-ID
             END-EXEC.

             ADD 1 TO PPI-DELETE-COUNT.
       C3000-EXIT.
             EXIT.
      /
       D1000-PRINT-RPT1-DETAIL.
      ******************************************************************
      *    WRITE RPT1 DETAIL LINE.
      ******************************************************************

           MOVE 'D1000-PRINT-RPT1-DETAIL' TO WS-ABEND-PARAGRAPH.

           IF RPT1-LINE-COUNT > 60
               PERFORM D1500-PRINT-RPT1-HEADING
           END-IF.

           WRITE RPT1-RECORD         FROM RPT1-DTL-LINE-1
                                    AFTER ADVANCING 1.

           ADD  1                      TO RPT1-LINE-COUNT.
           ADD  1                      TO RPT1-DETAIL-COUNT.


       D1500-PRINT-RPT1-HEADING.
      ******************************************************************
      *    WRITE RPT1 PAGE HEADINGS.
      ******************************************************************

           MOVE 'D1500-PRINT-RPT1-HEADING' TO WS-ABEND-PARAGRAPH.

           ADD  1                      TO RPT1-PAGE-COUNT.
           MOVE RPT1-PAGE-COUNT        TO RPT1-PAGE.

           WRITE RPT1-RECORD         FROM RPT1-HEADING-1
                                    AFTER ADVANCING PAGE.
           WRITE RPT1-RECORD         FROM RPT1-HEADING-2
                                    AFTER ADVANCING 1.
           WRITE RPT1-RECORD         FROM RPT1-HEADING-3
                                    AFTER ADVANCING 1.
           WRITE RPT1-RECORD         FROM RPT1-HEADING-4
                                    AFTER ADVANCING 1.
           WRITE RPT1-RECORD         FROM RPT1-DTL-HDG-1
                                    AFTER ADVANCING 2.
           WRITE RPT1-RECORD         FROM RPT1-DTL-HDG-2
                                    AFTER ADVANCING 1.

           MOVE 7                      TO RPT1-LINE-COUNT.
      /
       S1000-CHECK-ENI.
      ******************************************************************
      ******************************************************************

           MOVE 'S1000-CHECK-ENT' TO WS-ABEND-PARAGRAPH.
           MOVE  +1000                 TO WS-ABEND-SQL-NBR.

           INITIALIZE FPECURID-INP-ID-KEY.

           MOVE  PRS-EMP-ID TO FPECURID-INP-LST-NINE.
           MOVE 'E'         TO FPECURID-INP-PE-IND.
           MOVE 'Y'         TO FPECURID-INP-PE-SPECIFIC.

           CALL FPECURID-PROGRAM   USING FPECURID-PARM-AREA.

           IF FPECURID-ERROR
               GO TO S9999-DB2-ERROR
           END-IF.

           IF FPECURID-FOUND
               MOVE 'Y'                TO EMP-ID-FOUND-ENT-SW
               MOVE 'EMP ID FOUND AS AN ENTITY'
                                       TO RPT1-MESSAGE
           END-IF.

       S1000-EXIT.
           EXIT.
      /
       U2000-UPDATE-PRS.
      ******************************************************************
      *    UPDATE PRS ROW FOR THE PERSON.
      ******************************************************************

           MOVE 'U2000-UPDATE-PRS'     TO WS-ABEND-PARAGRAPH.
           MOVE  +4000                 TO WS-ABEND-SQL-NBR.

           MOVE WS-THIS-PROGRAM        TO PRS-USER-CD.
           MOVE SPACES TO PRS-EMP-ID.

           EXEC SQL
               UPDATE  PRS_PRSN_V
                  SET  USER_CD          = :PRS-USER-CD
                      ,EMP_ID           = :PRS-EMP-ID
                WHERE  IREF_ID          = :PRS-IREF-ID
           END-EXEC.

           MOVE SQLCODE                TO PRS-SQLCODE.

           IF  SQLCODE = +0
               ADD 1                   TO PRS-UPDATE-COUNT
           ELSE
               PERFORM S9997-ROLLBACK-UPDATES
               GO TO S9999-DB2-ERROR.


       U2000-EXIT.
            EXIT.

      /
       S9997-ROLLBACK-UPDATES.
      ******************************************************************
      *    BACK OUT ALL DB2 UPDATES SINCE THE LAST COMMIT.
      ******************************************************************

           EXEC SQL
               ROLLBACK
           END-EXEC.

           DISPLAY 'UPDATES ROLLED BACK FOR'
                  ' SSN# '  PRS-SSN-LST-NINE.

           MOVE +0                     TO WS-COMMIT-COUNT.


       S9998-COMMIT-UPDATES.
      ******************************************************************
      *    COMMIT ALL DB2 UPDATES UP TO THIS POINT.
      ******************************************************************

           EXEC SQL
               COMMIT
           END-EXEC.

           MOVE +0                     TO WS-COMMIT-COUNT.
      /
       S9999-DB2-ERROR.
      ******************************************************************
      *    DB2ERRBP - DB2-ERROR-ROUTINE   (DB2 ERROR HANDLING ROUTINE)
      *    DB2ERRBP DISPLAYS DB2 ERROR MSG TO SYSOUT, ROLLSBACK UPDATES,
      *    SETS RETURN-CODE TO '9', AND CALLS 'ABORT' TO PRODUCE A DUMP.
      ******************************************************************

           MOVE WS-THIS-PROGRAM        TO DB2-PROGRAM-NAME.
           MOVE WS-ABEND-SQL-NBR       TO DB2-DML-NUMBER.
           MOVE SQLCODE                TO WS-ABEND-CODE.

           DISPLAY 'ABNORMAL DB2 ERROR DETECTED IN PROGRAM '
                                          DB2-PROGRAM-NAME
           DISPLAY 'LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY 'PARAGRAPH / SQL NUMBER = '
                                          DB2-DML-NUMBER.
           DISPLAY 'DB2 SQLCODE = '       WS-ABEND-CODE.

           DISPLAY 'ABEND OCCURRED FOR'
                  ' SSN# '  PRS-SSN-LST-NINE
                 ', PID# '  PPI-PRSN-ID-LST-NINE.

           EXEC SQL INCLUDE DB2ERRBP
           END-EXEC.
      /
       Z9999-ABORT-PROGRAM.
      ******************************************************************
      *    AN ABNORMAL CONDITION HAS OCCURED.  ABORT PROGRAM PROCESSING.
      ******************************************************************

           DISPLAY ' '.
           DISPLAY '**************************************************'.
           DISPLAY '** ABNORMAL ERROR DETECTED IN PROGRAM '
                                          WS-THIS-PROGRAM.
           DISPLAY '** ERROR MESSAGE IS DISPLAYED BEFORE THIS TEXT BOX'.
           DISPLAY '** LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY '** PROGRAM PROCESSING IS BEING ABORTED'.
           DISPLAY '**************************************************'.

           MOVE '9' TO RETURN-CODE.
           CALL 'ABORT'.
