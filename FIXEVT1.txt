       IDENTIFICATION DIVISION.
       PROGRAM-ID.    FIXEVT1.
      ******************************************************************
      *    TITLE ......... FIX TOTAL ADVANCE AMOUNT ON EVT TABLE
      *    LANGUAGE ...... COBOL II / BATCH / DB2
      *    AUTHOR ........ M.MCGILL
      *    DATE-WRITTEN .. OCTOBER, 2007
      *
      * 1. FUNCTION:
      *    THE PURPOSE OF THIS PROGRAM IS TO FIX THE TOTL_ADVNC_AMT
      *    COLUMN ON THE EVT TABLE WAS WAS INCORRECTLY DUPLICATED
      *
      * 2. FILES:
      *    INPUT:   ADV_ADVNC_V            (DB2)
      *             EVT_EVENT_V            (DB2)
      *
      *    OUTPUT:  EVT_EVENT_V            (DB2)
      *
      * 3. REPORTS: CRPT CONTROL REPORT
      *
      * 4. PROCESSING LOGIC:
      *
      *    PROCESS THE DB2 CURSOR TO RETRIEVE INFORMATION FROM THE
      *    ADV_ADVNC_V TABLE.
      *
      *    ORDER AND PROCESS ROWS IN THE FOLLOWING SEQUENCE GROUPS:
      *
      *    A) ADV-FK-EVT-EVNT-NBR (BREAK-1):
      *          GET ASSOCIATED INFORMATION FROM TABLE EVT.
      *
      *    B) PROCESS-ROW:
      *          SUMMARIZE ADV-ADVNC-AMT FOR THE BREAK-1 GROUP.
      ******************************************************************

      ******************************************************************
      *                      MAINTENANCE HISTORY                       *
      ******************************************************************
      *    MODIFIED BY .....                                           *
      *    DATE MODIFIED ...                                           *
      *    MODIFICATION ....                                           *
      ******************************************************************
      /
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT CRPT-FILE            ASSIGN CNTLRPT.

       DATA DIVISION.
       FILE SECTION.

       FD  CRPT-FILE
           RECORDING MODE F
           BLOCK CONTAINS 0 RECORDS
           LABEL RECORDS OMITTED.
       01  CRPT-RECORD                 PIC X(132).
      /
       WORKING-STORAGE SECTION.
      ******************************************************************
      *    MISCELLANEOUS WORK AREAS
      ******************************************************************
       01  FILLER                      PIC X(36)       VALUE
           'FIXEVT1 WORKING-STORAGE BEGINS HERE'.

       01  WS-CONSTANTS.
           05  WS-THIS-PROGRAM         PIC X(08)       VALUE 'FIXEVT1'.

       01  WS-ABEND-AREA.
           05  WS-ABEND-PARAGRAPH      PIC X(30)       VALUE SPACES.
           05  WS-ABEND-SQL-NBR        PIC S9(4)       VALUE ZEROES.
           05  WS-ABEND-CODE           PIC -999        VALUE ZEROES.

       01  WS-DATE-AREA.
           05  WS-SYS-DATE.
               10  WS-SYS-DATE-CC      PIC 9(02)       VALUE ZEROES.
               10  WS-SYS-DATE-YYMMDD.
                   15  WS-SYS-DATE-YY  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-MM  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-DD  PIC 9(02)       VALUE ZEROES.
           05  WS-SYSTEM-DATE          REDEFINES WS-SYS-DATE
                                       PIC 9(08).
           05  WS-RPT-DATE.
               10  WS-RPT-DATE-MM      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-DD      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-YY      PIC X(02)       VALUE SPACES.

       01  WS-WORK-AREA.
           05  WS-ADV-FK-EVT-EVNT-NBR  PIC X(08)       VALUE SPACES.
           05  WS-EVT-TOTL-ADVNC-AMT   PIC S9(9)V99    COMP-3
                                                       VALUE ZEROES.
           05  WS-TOT-ADVNC-AMT        PIC S9(9)V99    COMP-3
                                                       VALUE ZEROES.
           05  WS-MISMATCH-COUNT       PIC 9(03)       VALUE ZEROES.
           05  WS-COMPUTE-ADV-AMT      PIC S9(9)V99    COMP-3
                                                       VALUE ZEROES.
      /
      ******************************************************************
      *    CONTROL REPORT AREA
      ******************************************************************
       01  DATE-CONV-COB2.             COPY WSDATEC2.

       01  TIME-CONV-COB2.             COPY WSTIMEC2.

       01  WHEN-COMP-CONV.             COPY WHENCMC2.

       01  CRPT-COUNTERS.
           05  CRPT-PAGE-COUNT         PIC 9(03)       VALUE ZEROES.
           05  CRPT-LINE-COUNT         PIC 9(03)       VALUE 99.

       01  CRPT-HEADING-1.
           05  FILLER                  PIC X(08)       VALUE 'FIXEVT1'.
           05  FILLER                  PIC X(40)       VALUE SPACES.
           05  FILLER                  PIC X(35)       VALUE
               'UNIVERSITY OF CALIFORNIA, SAN DIEGO'.
           05  FILLER                  PIC X(35)       VALUE SPACES.
           05  CRPT-PAGE               PIC Z(04)       VALUE ZEROES.
           05  FILLER                  PIC X(10)       VALUE SPACES.

       01  CRPT-HEADING-2.
           05  FILLER                  PIC X(07)       VALUE 'RUN ON'.
           05  CRPT-RUN-DATE           PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(04)       VALUE ' AT'.
           05  CRPT-RUN-TIME.
               10  CRPT-RUN-TIME-HH    PIC 9(02)       VALUE ZEROES.
               10  FILLER              PIC X(01)       VALUE ':'.
               10  CRPT-RUN-TIME-MM    PIC 9(02)       VALUE ZEROES.
               10  FILLER              PIC X(01)       VALUE ':'.
               10  CRPT-RUN-TIME-SS    PIC 9(02)       VALUE ZEROES.
           05  FILLER                  PIC X(27)       VALUE SPACES.
           05  FILLER                  PIC X(78)       VALUE
               'BATCH REQUEST FILE LOAD '.

       01  CRPT-HEADING-3.
           05  FILLER                  PIC X(59)       VALUE SPACES.
           05  FILLER                  PIC X(14)       VALUE
               'CONTROL REPORT'.
           05  FILLER                  PIC X(59)       VALUE SPACES.

       01  CRPT-DTL-LINE-1             PIC X(132)      VALUE SPACES.
      /
      ******************************************************************
      *    DB2 ERROR HANDLING WORK AREA
      ******************************************************************

           EXEC SQL INCLUDE DB2ERRBW
           END-EXEC.
      /
      ******************************************************************
      *    SQL AREA AND TABLE DB2 TABLE DCLGENS
      ******************************************************************

           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
       01  ADV-ADVNC-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'ADV*'.
           05  ADV-COUNT               PIC 9(08)       VALUE ZEROES.
           05  ADV-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVADV
           END-EXEC.
      /
       01  EVT-EVENT-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'EVT*'.
           05  EVT-COUNT               PIC 9(08)       VALUE ZEROES.
           05  EVT-UPDATE-COUNT        PIC 9(08)       VALUE ZEROES.
           05  EVT-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVEVT
           END-EXEC.
      /
       PROCEDURE DIVISION.
      /
       A0000-MAINLINE.
      ******************************************************************
      *    PERFORM THE PROGRAM'S MAINLINE FUNCTIONS.
      ******************************************************************

           PERFORM A1000-INITIALIZATION.

           PERFORM B1000-MAIN-PROCESS.

           PERFORM A8000-FINALIZATION.

           GOBACK.
      /
       A1000-INITIALIZATION.
      ******************************************************************
      *    OPEN FILES AND INITIALIZE WORKING STORAGE AREAS.
      ******************************************************************

           MOVE 'A1000-INITIALIZATION' TO WS-ABEND-PARAGRAPH.

           DISPLAY '>>>>  FIXEVT1 STARTING  <<<<'.

           OPEN OUTPUT CRPT-FILE.

           ADD 0 TO WS-MISMATCH-COUNT.

           MOVE FUNCTION CURRENT-DATE(1:8)
                                       TO WS-SYS-DATE.
           MOVE WS-SYS-DATE-YY         TO WS-RPT-DATE-YY.
           MOVE WS-SYS-DATE-MM         TO WS-RPT-DATE-MM.
           MOVE WS-SYS-DATE-DD         TO WS-RPT-DATE-DD.
           MOVE WS-RPT-DATE            TO CRPT-RUN-DATE.

           COPY PDTIMEC2.

           MOVE TIME-HH                TO CRPT-RUN-TIME-HH.
           MOVE TIME-MM                TO CRPT-RUN-TIME-MM.
           MOVE TIME-SS                TO CRPT-RUN-TIME-SS.

           MOVE WHEN-COMPILED          TO COMPILED-DATA.
           INSPECT COMPILED-DATA-TIME
                   REPLACING ALL '.' BY ':'.
           STRING 'PROGRAM COMPILED '     COMPILED-DATA-DATE
                  ' AT '                  COMPILED-DATA-TIME
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.

           PERFORM A9000-PRINT-CRPT-DETAIL.
      /
       A8000-FINALIZATION.
      ******************************************************************
      *    REPORT/DISPLAY FINAL TOTALS AND CLOSE FILES.
      ******************************************************************

           MOVE 'A8000-FINALIZATION'   TO WS-ABEND-PARAGRAPH.

           COPY PDTIMEC2.

           MOVE 'PROGRAM STATUS'       TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF ADV-CURSOR ROWS FETCHED   =>  '
                   ADV-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF EVT ROWS SELECTED         =>  '
                   EVT-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF EVT ROWS UPDATED          =>  '
                   EVT-UPDATE-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING '# OF EVENTS WITH MIS-MATCHED AMT =>  '
                   WS-MISMATCH-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'PROGRAM HAS BEEN SUCCESSFULLY COMPLETED AT '
                   TIME-HH ':' TIME-MM
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           MOVE ALL 'END OF REPORT '   TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           CLOSE CRPT-FILE.

           DISPLAY '>>>>  FIXEVT1 FINISHED  <<<<'.
      /
       A9000-PRINT-CRPT-DETAIL.
      ******************************************************************
      *    WRITE CONTROL REPORT DETAIL LINE.
      ******************************************************************

           MOVE 'A9000-PRINT-CRPT-DETAIL' TO WS-ABEND-PARAGRAPH.

           IF CRPT-LINE-COUNT > 55
               PERFORM A9500-PRINT-CRPT-HEADING
           END-IF.

           WRITE CRPT-RECORD         FROM CRPT-DTL-LINE-1
                                    AFTER ADVANCING 2.

           MOVE SPACES                 TO CRPT-DTL-LINE-1.
           ADD  2                      TO CRPT-LINE-COUNT.


       A9500-PRINT-CRPT-HEADING.
      ******************************************************************
      *    WRITE CONTROL REPORT PAGE HEADINGS.
      ******************************************************************

           MOVE 'A9500-PRINT-CRPT-HEADING' TO WS-ABEND-PARAGRAPH.

           ADD  1                      TO CRPT-PAGE-COUNT.
           MOVE CRPT-PAGE-COUNT        TO CRPT-PAGE.

           WRITE CRPT-RECORD         FROM CRPT-HEADING-1
                                    AFTER ADVANCING PAGE.
           WRITE CRPT-RECORD         FROM CRPT-HEADING-2
                                    AFTER ADVANCING 1.
           WRITE CRPT-RECORD         FROM CRPT-HEADING-3
                                    AFTER ADVANCING 1.

           MOVE 3                      TO CRPT-LINE-COUNT.
      /
       B1000-MAIN-PROCESS.
      ******************************************************************
      *    PROCESS THE ADV-CURSOR TO SUMMARIZE THE ADV-ADVNC-AMT.
      ******************************************************************

           MOVE 'B1000-MAIN-PROCESS'   TO WS-ABEND-PARAGRAPH.

           PERFORM S1000-OPEN-ADV-CURSOR.

           PERFORM S1010-FETCH-ADV-CURSOR.

           PERFORM B2000-PROCESS-ADV-CURSOR
               UNTIL ADV-SQLCODE = +100.

           PERFORM S1020-CLOSE-ADV-CURSOR.
      /
       B2000-PROCESS-ADV-CURSOR.
      ******************************************************************
      *    FOR EACH TABLE ADV CONTROL BREAK:
      *    1)  UPDATE TABLE EVT TOTL_ADVNC_AMT.
      ******************************************************************

           MOVE 'B2000-PROCESS-ADV-BREAK' TO WS-ABEND-PARAGRAPH.

           MOVE ADV-FK-EVT-EVNT-NBR    TO EVT-EVENT-NBR.
           MOVE ADV-FK-TVL-IREF-ID     TO EVT-FK-TVL-IREF-ID.
           MOVE ADV-FK-TVL-ENTPSN-IND  TO EVT-FK-TVL-ENTPSN-IND.
           PERFORM S2000-SELECT-EVT.

           MOVE ADV-FK-EVT-EVNT-NBR    TO WS-ADV-FK-EVT-EVNT-NBR.
           MOVE ZEROES                 TO WS-TOT-ADVNC-AMT.

           PERFORM B3000-PROCESS-ROWS
               UNTIL ADV-SQLCODE             = +100
                  OR ADV-FK-EVT-EVNT-NBR NOT = WS-ADV-FK-EVT-EVNT-NBR.

           COMPUTE WS-COMPUTE-ADV-AMT
                 = WS-TOT-ADVNC-AMT
                 * +2
           END-COMPUTE.

           IF WS-COMPUTE-ADV-AMT = EVT-TOTL-ADVNC-AMT
              DISPLAY ' *****************************************'
              DISPLAY ' ** AMOUNT IS EXACTLY DOUBLED FOR '
              DISPLAY ' EVENT NUMBER ' WS-ADV-FK-EVT-EVNT-NBR
              DISPLAY ' WS-TOT-ADVNC-AMT = ' WS-TOT-ADVNC-AMT
              DISPLAY ' TOTALED ADV ADV AMT * 2 ' WS-COMPUTE-ADV-AMT
              DISPLAY ' EVT TOT AMT ' EVT-TOTL-ADVNC-AMT
              PERFORM S3000-UPDATE-EVT-TOT-AMT
           ELSE
              ADD 1 TO WS-MISMATCH-COUNT
              DISPLAY ' *****************************************'
              DISPLAY ' ** AMOUNT IS NOT EXACTLY DOUBLED FOR '
              DISPLAY ' EVENT NUMBER ' WS-ADV-FK-EVT-EVNT-NBR
              DISPLAY ' WS-TOT-ADVNC-AMT = ' WS-TOT-ADVNC-AMT
              DISPLAY ' TOTALED ADV ADV AMT * 2 ' WS-COMPUTE-ADV-AMT
              DISPLAY ' EVT TOT AMT ' EVT-TOTL-ADVNC-AMT
              PERFORM S3000-UPDATE-EVT-TOT-AMT
           END-IF.
      /
       B3000-PROCESS-ROWS.
      ******************************************************************
      *    SUMMARIZE ADV-ADVNC-AMT FOR THE CONTROL BREAK.
      *    FETCH THE NEXT ROW.
      ******************************************************************

           MOVE 'B3000-PROCESS-ROWS'   TO WS-ABEND-PARAGRAPH.

           ADD  ADV-ADVNC-AMT          TO WS-TOT-ADVNC-AMT.

           PERFORM S1010-FETCH-ADV-CURSOR.
      /
       S1000-OPEN-ADV-CURSOR.
      ******************************************************************
      *    OPEN THE ADV-CURSOR FOR ADV-ADVNC.
      ******************************************************************

           MOVE 'S1000-OPEN-ADV-CURSOR'    TO WS-ABEND-PARAGRAPH.
           MOVE  +1000                     TO WS-ABEND-SQL-NBR.

           EXEC SQL
             DECLARE   ADV-CURSOR CURSOR FOR
               SELECT  ADV.USER_CD
                      ,ADV.LAST_ACTVY_DT
                      ,ADV.ADVNC_AMT
                      ,ADV.FK_TVL_IREF_ID
                      ,ADV.FK_TVL_ENTPSN_IND
                      ,ADV.FK_EVT_EVNT_NBR
                 FROM  ADV_ADVNC_V ADV
                WHERE ADV.USER_CD           = 'UTAU350'
                  AND ADV.LAST_ACTVY_DT     = '07257'
                ORDER BY
                       ADV.FK_EVT_EVNT_NBR
           END-EXEC.

           EXEC SQL
             OPEN ADV-CURSOR
           END-EXEC.

           MOVE SQLCODE                TO ADV-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S1010-FETCH-ADV-CURSOR.
      ******************************************************************
      *    GET THE NEXT ROW FOR ADV-CURSOR.
      ******************************************************************

           MOVE 'S1010-FETCH-ADV-CURSOR'   TO WS-ABEND-PARAGRAPH.
           MOVE  +1010                     TO WS-ABEND-SQL-NBR.

           EXEC SQL
               FETCH   ADV-CURSOR
                INTO  :ADV-USER-CD
                     ,:ADV-LAST-ACTVY-DT
                     ,:ADV-ADVNC-AMT
                     ,:ADV-FK-TVL-IREF-ID
                     ,:ADV-FK-TVL-ENTPSN-IND
                     ,:ADV-FK-EVT-EVNT-NBR
           END-EXEC.

           MOVE SQLCODE                TO ADV-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO ADV-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S1020-CLOSE-ADV-CURSOR.
      ******************************************************************
      *    CLOSE THE ADV-CURSOR.
      ******************************************************************

           MOVE 'S1020-CLOSE-ADV-CURSOR'   TO WS-ABEND-PARAGRAPH.
           MOVE  +1020                     TO WS-ABEND-SQL-NBR.

           EXEC SQL
               CLOSE   ADV-CURSOR
           END-EXEC.

           MOVE SQLCODE                TO ADV-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S2000-SELECT-EVT.
      ******************************************************************
      *    GET TABLE EVT TOTL_ADVNC_AMT.
      ******************************************************************

           MOVE 'S2000-SELECT-EVT'     TO WS-ABEND-PARAGRAPH.
           MOVE  +2000                 TO WS-ABEND-SQL-NBR.

           EXEC SQL
               SELECT  EVT.TOTL_ADVNC_AMT
                 INTO :EVT-TOTL-ADVNC-AMT
                 FROM  EVT_EVENT_V EVT
                WHERE  EVT.EVENT_NBR         = :EVT-EVENT-NBR
                  AND  EVT.FK_TVL_IREF_ID    = :EVT-FK-TVL-IREF-ID
                  AND  EVT.FK_TVL_ENTPSN_IND = :EVT-FK-TVL-ENTPSN-IND
           END-EXEC.

           MOVE SQLCODE                TO EVT-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO EVT-COUNT
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S3000-UPDATE-EVT-TOT-AMT.
      ******************************************************************
      *    UPDATE TABLE EVT TOTL_ADVNC_AMT.
      ******************************************************************

           MOVE 'S3000-UPDATE-EVT-TOT-AMT' TO WS-ABEND-PARAGRAPH.
           MOVE  +3000                     TO WS-ABEND-SQL-NBR.

           COMPUTE WS-EVT-TOTL-ADVNC-AMT
                 = EVT-TOTL-ADVNC-AMT
                 - WS-TOT-ADVNC-AMT
           END-COMPUTE.

           DISPLAY ' *****************************************'.
           DISPLAY ' EVT EVENT NUMBER ' EVT-EVENT-NBR.
           DISPLAY ' TOT ADVANCE AMOUNT ' EVT-TOTL-ADVNC-AMT.
           DISPLAY ' BEING CHANGED TO   ' WS-EVT-TOTL-ADVNC-AMT.

           DISPLAY ' WS-EVT-TOTL-ADVNC-AMT = '
                     WS-EVT-TOTL-ADVNC-AMT.

           EXEC SQL
               UPDATE EVT_EVENT_V
                   SET TOTL_ADVNC_AMT    = :WS-EVT-TOTL-ADVNC-AMT
                 WHERE EVENT_NBR         = :EVT-EVENT-NBR
                   AND FK_TVL_IREF_ID    = :EVT-FK-TVL-IREF-ID
                   AND FK_TVL_ENTPSN_IND = :EVT-FK-TVL-ENTPSN-IND
           END-EXEC.

           MOVE SQLCODE                TO EVT-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO EVT-UPDATE-COUNT
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S9999-DB2-ERROR.
      ******************************************************************
      *    DB2ERRBP - DB2-ERROR-ROUTINE   (DB2 ERROR HANDLING ROUTINE)
      *    DB2ERRBP DISPLAYS DB2 ERROR MSG TO SYSOUT, ROLLSBACK UPDATES,
      *    SETS RETURN-CODE TO '9', AND CALLS 'ABORT' TO PRODUCE A DUMP.
      ******************************************************************

           MOVE WS-THIS-PROGRAM        TO DB2-PROGRAM-NAME.
           MOVE WS-ABEND-SQL-NBR       TO DB2-DML-NUMBER.
           MOVE SQLCODE                TO WS-ABEND-CODE.

           DISPLAY 'ABNORMAL DB2 ERROR DETECTED IN PROGRAM '
                                          DB2-PROGRAM-NAME
           DISPLAY 'LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY 'PARAGRAPH / SQL NUMBER = '
                                          DB2-DML-NUMBER.
           DISPLAY 'DB2 SQLCODE = '       WS-ABEND-CODE.


           EXEC SQL INCLUDE DB2ERRBP
           END-EXEC.
      /
       Z9999-ABORT-PROGRAM.
      ******************************************************************
      *    AN ABNORMAL CONDITION HAS OCCURED.  ABORT PROGRAM PROCESSING.
      ******************************************************************

           DISPLAY ' '.
           DISPLAY '**************************************************'.
           DISPLAY '** ABNORMAL ERROR DETECTED IN PROGRAM '
                                          WS-THIS-PROGRAM.
           DISPLAY '** ERROR MESSAGE IS DISPLAYED BEFORE THIS TEXT BOX'.
           DISPLAY '** LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY '** PROGRAM PROCESSING IS BEING ABORTED'.
           DISPLAY '**************************************************'.

           MOVE '9' TO RETURN-CODE.
           CALL 'ABORT'.
