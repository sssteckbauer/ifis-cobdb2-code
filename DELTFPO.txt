       IDENTIFICATION DIVISION.                                         00010008
       PROGRAM-ID. DELTFPO.
       AUTHOR. D WRIGHT                                                 00030008
       DATE-WRITTEN.                                                    00040008
       DATE-COMPILED.                                                   00050008
       ENVIRONMENT DIVISION.                                            00420008
       CONFIGURATION SECTION.                                           00430008
       SPECIAL-NAMES.                                                   00440008
           C01 IS TOP-OF-PAGE.                                          00450008
       SOURCE-COMPUTER.                                                 00460008
       OBJECT-COMPUTER.                                                 00470008
                                                                        00480008
       INPUT-OUTPUT SECTION.                                            00490008
       FILE-CONTROL.                                                    00500008
                                                                        00550008
           SELECT  PO-FILE-IN    ASSIGN TO POFILEIN.
           SELECT  PO-FILE-OT    ASSIGN TO POFILEOT.
           SELECT  REPORTFILE    ASSIGN TO POREPORT.
                                                                        00570008
       DATA DIVISION.                                                   00640008
       FILE SECTION.                                                    00680008
                                                                        00690008
                                                                        01090008
       FD  PO-FILE-IN
           RECORDING MODE IS F
           LABEL RECORDS ARE OMITTED
           BLOCK CONTAINS 0 RECORDS
           RECORD CONTAINS 100 CHARACTERS
           DATA RECORD IS PO-RECORD.
       01  PO-RECORD       PIC X(100).

       FD  PO-FILE-OT
           RECORDING MODE IS F
           LABEL RECORDS ARE OMITTED
           BLOCK CONTAINS 0 RECORDS
           RECORD CONTAINS 20 CHARACTERS
           DATA RECORD IS PO-RECORD-OT.
       01  PO-RECORD-OT            PIC X(20).

       FD  REPORTFILE
           LABEL RECORDS ARE STANDARD.
       01  REPORT02-RECORD                 PIC X(133).

       WORKING-STORAGE SECTION.
       01  FILLER  PIC X(32) VALUE '** DELTFPO WORKING STRG BEGINS'.
       01  WS-PAGE                 PIC 9(02) VALUE 1.
       01  WS-END-OF-PO            PIC X(01) VALUE 'N'.
       01  WS-CHG-ORD-NEEDED-SW    PIC X(01) VALUE 'N'.
       01  WS-END-OF-POD-SW        PIC X(01) VALUE 'N'.
       01  DBLINK-USER-CD          PIC X(8) VALUE SPACES.
       01  WS-CREDIT-AMT           PIC S9(10)V9(2) USAGE COMP-3.
       01  WS-DEBIT-AMT            PIC S9(10)V9(2) USAGE COMP-3.
       01  WS-ROW-EMPTY-IND   COMP      PIC S9(04) VALUE +0.
       01  ORDER-DATE-4096              COMP-3    PIC S9(8).
       01  BLNKT-TERM-DATE-4083         COMP-3    PIC S9(8).
       01  LAST-INV-PAID-DATE           COMP-3    PIC S9(8).
       01  WS-LINE-CTR                      PIC  9(03) VALUE 60.
       01  WS-PERCENT              PIC S999V999 VALUE 0.
       01  WS-REMAIN-BAL           PIC S9999V99 VALUE 0.
       01  WS-PO-PURGE-EXTR.
           03  EXT-KEY.
               05  EXT-UNVRS-CODE           PIC X(02).
               05  EXT-PO-NMBR              PIC X(08).
               05  EXT-REC-TYPE             PIC X(05).
               05  EXT-DBKEY                PIC S9(08) COMP.
               05  EXT-FILLER               PIC X(01).

       01  WS-PO-RECORD.
           05  IN-PO-NBR           PIC X(08).
           05  FILLER1             PIC X(01) VALUE SPACES.
           05  IN-PO-CLS           PIC X(01).
           05  FILLER20            PIC X(01) VALUE SPACES.
           05  IN-CHNG-SEQ-NBR     PIC X(03).
           05  FILLER2             PIC X(01) VALUE SPACES.
           05  IN-PO-STATUS        PIC X(01).
           05  FILLER3             PIC X(01) VALUE SPACES.
           05  IN-ORDER-DT         PIC X(10).
           05  FILLER4             PIC X(01) VALUE SPACES.
           05  IN-BLNKT-TERM-DT    PIC X(10).
           05  FILLER5             PIC X(01) VALUE SPACES.
           05  IN-PO-AMT           PIC X(12).
           05  FILLER6             PIC X(01) VALUE SPACES.
           05  IN-INV-AMT          PIC X(12).
           05  FILLER7             PIC X(01) VALUE SPACES.
           05  IN-ENC-AMT          PIC X(12).
           05  FILLER8             PIC X(01) VALUE SPACES.
           05  IN-PERCENT-PAID     PIC X(06).
           05  FILLER9             PIC X(01) VALUE SPACES.
           05  IN-ENCY-LIFTED-YR   PIC X(02).
           05  FILLER10            PIC X(01) VALUE SPACES.
           05  IN-DATE-TIME-TS     PIC X(10).

       01  WS-ACCEPT-DATE.                                              07820000
           03  WS-ACCEPT-DATE-CC          PIC 9(02).                    07830000
           03  WS-ACCEPT-DATE-YY          PIC 9(02).                    07830000
           03  WS-ACCEPT-DATE-MM          PIC 9(02).                    07840000
           03  WS-ACCEPT-DATE-DD          PIC 9(02).                    07850000
                                                                        07860000
       01  WS-ACCEPT-DATE2.                                             07870000
           03  WS-ACCEPT-DATE-MM          PIC 9(02).                    07880000
           03  FILLER                     PIC X(01) VALUE '/'.          07890000
           03  WS-ACCEPT-DATE-DD          PIC 9(02).                    07900000
           03  FILLER                     PIC X(01) VALUE '/'.          07910000
           03  WS-ACCEPT-DATE-YY          PIC 9(02).                    07920000
                                                                        07930000
       01  WS-ACCEPT-TIME.                                              07940000
           03  WS-ACCEPT-TIME-HRS         PIC X(02).                    07950000
           03  WS-ACCEPT-TIME-MIN         PIC X(02).                    07960000
           03  WS-ACCEPT-TIME-SEC         PIC X(02).                    07970000
                                                                        07980000
       01  WS-ACCEPT-TIME2.                                             07990000
           03  WS-ACCEPT-TIME-HRS         PIC X(02).                    08000000
           03  FILLER                     PIC X(01) VALUE ':'.          08010000
           03  WS-ACCEPT-TIME-MIN         PIC X(02).                    08020000
           03  FILLER                     PIC X(01) VALUE ':'.          08030000
           03  WS-ACCEPT-TIME-SEC         PIC X(02).                    08040000
                                                                        08050000
       01  HEADING-1.
              05  FILLER                     PICTURE X(01) VALUE SPACES.09100000
              05  H1-REPORT-NO               PICTURE X(08) VALUE        09110000
                  'FAIP PO '.                                           09120000
              05  FILLER                     PICTURE X(35) VALUE SPACES.09130000
              05  FILLER                     PICTURE X(35) VALUE        09140000
              'UNIVERSITY OF CALIFORNIA, SAN DIEGO'.                    09150000
              05  FILLER                     PICTURE X(35) VALUE SPACES.09160000
              05  FILLER                     PICTURE X(06) VALUE        09170000
                  'PAGE: '.                                             09180000
              05  H1-PAGE-NMBR               PICTURE ZZZ9.              09190000
              05  FILLER                     PICTURE X(01) VALUE SPACES.09200000
           03  HEADING-2.                                               09210000
              05  FILLER                     PICTURE X(01) VALUE SPACES.09220000
              05  FILLER                     PICTURE X(09) VALUE        09230000
                  'RUN ON: '.                                           09240000
              05  H2-RUN-DATE                PICTURE X(08) VALUE SPACES.09250000
              05  FILLER                     PICTURE X(04) VALUE '    '.09260000
              05  H2-RUN-TIME-HH             PIC 9(002).                09270000
              05  FILLER                     PIC X(001) VALUE ' '.      09280000
              05  H2-RUN-TIME-MM             PIC 9(002).                09290000
              05  FILLER                     PIC X(001) VALUE ' '.      09300000
              05  H2-RUN-TIME-SS             PIC 9(002).                09310000
              05  FILLER                     PICTURE X(15) VALUE SPACES.09320000
              05  FILLER                     PICTURE X(36) VALUE        09330000
              'PO WITH ACCOUNT 637200 OR 638000   '.                    09340000
              05  FILLER                     PICTURE X(27) VALUE SPACES.09350000
              05  FILLER                     PICTURE X(30) VALUE SPACES.09360000

       01  HEADING-3.
             05 FILLER             PIC X(01).
             05 FILLER             PIC X(08) VALUE '   PO   '.
             05 FILLER             PIC X(02).
             05 FILLER             PIC X(01) VALUE ' '.
             05 FILLER             PIC X(02).
             05 FILLER             PIC X(03) VALUE 'CHG'.
             05 FILLER             PIC X(03).
             05 FILLER             PIC X(01) VALUE 'S'.
             05 FILLER             PIC X(02).
             05 FILLER             PIC X(10) VALUE ' PO ORDER '.
             05 FILLER             PIC X(03).
             05 FILLER             PIC X(10) VALUE ' BLNKT ORD'.
             05 FILLER             PIC X(03).
             05 FILLER             PIC X(13) VALUE '      PO    '.
             05 FILLER             PIC X(01).
             05 FILLER             PIC X(13) VALUE '      INV   '.
             05 FILLER             PIC X(01).
             05 FILLER             PIC X(13) VALUE '      ENC   '.
             05 FILLER             PIC X(05) VALUE ' PAID'.
             05 FILLER             PIC X(01) VALUE ' '.
             05 FILLER             PIC X(02).
             05 FILLER             PIC X(03)  VALUE 'ENC'.
             05 FILLER             PIC X(01) VALUE SPACES.
             05 FILLER             PIC X(10) VALUE ' INV LAST'.

       01  HEADING-4.
             05 FILLER             PIC X(01).
             05 FILLER             PIC X(08) VALUE ' NUMBER '.
             05 FILLER             PIC X(01).
             05 FILLER             PIC X(03) VALUE 'CLS'.
             05 FILLER             PIC X(01).
             05 FILLER             PIC X(03) VALUE 'ORD'.
             05 FILLER             PIC X(03).
             05 FILLER             PIC X(01) VALUE 'T'.
             05 FILLER             PIC X(02).
             05 FILLER             PIC X(10) VALUE '   DATE   '.
             05 FILLER             PIC X(03).
             05 FILLER             PIC X(10) VALUE '   DATE   '.
             05 FILLER             PIC X(03).
             05 FILLER             PIC X(13) VALUE '    AMOUNT  '.
             05 FILLER             PIC X(01).
             05 FILLER             PIC X(13) VALUE '    AMOUNT  '.
             05 FILLER             PIC X(01).
             05 FILLER             PIC X(13) VALUE '    AMOUNT  '.
             05 FILLER             PIC X(05) VALUE ' PCNT'.
             05 FILLER             PIC X(01) VALUE '%'.
             05 FILLER             PIC X(02).
             05 FILLER             PIC X(03)  VALUE 'FWD'.
             05 FILLER             PIC X(01) VALUE SPACES.
             05 FILLER             PIC X(12) VALUE 'ACTVY DATE'.

       01  DETAIL-LINE             PIC X(130) VALUE SPACES.
       01  BLANK-LINE              PIC X(130) VALUE SPACES.
       01  HD-PO-HEADER-RECORD.
           03 PO-FILED.
             05 FILLER             PIC X(01).
             05 HD-PO-NBR          PIC X(08).
             05 FILLER             PIC X(02).
             05 HD-PO-CLS          PIC X(01).
             05 FILLER             PIC X(02).
             05 HD-CHNG-SEQ-NBR    PIC X(03).
             05 FILLER             PIC X(02).
             05 HD-PO-STATUS       PIC X(01).
             05 FILLER             PIC X(02).
             05 HD-ORDER-DT        PIC X(10).
             05 FILLER             PIC X(03).
             05 HD-BLNKT-TERM-DT   PIC X(10).
           03  AMOUNT-FILD.
             05 FILLER             PIC X(03).
             05 HD-PO-AMT          PIC X(13).
             05 FILLER             PIC X(01).
             05 HD-INV-AMT         PIC X(13).
             05 FILLER             PIC X(01).
             05 HD-ENC-AMT         PIC X(13).

             05 HD-PERCENT-PAID    PIC X(06).
             05 HD-PERCENT-SIGN    PIC X(01) VALUE '%'.
           03  FILLER              PIC X(02) VALUE SPACES.
           03  HD-ENCY-LIFTED-YR   PIC X(02).
           03  FILLER              PIC X(02) VALUE SPACES.
           03  HD-DATE-TIME-TS     PIC X(10).

       01  PL-PO-ITEM-RECORD.
           03 ITEM-LINE.
             05 FILLER             PIC X(34) VALUE SPACES.
             05 PL-ITEM-NBR        PIC 9(04).
             05 FILLER             PIC X(01) VALUE SPACES.
             05 PL-COMMDY-CODE     PIC X(08).
             05 FILLER             PIC X(01) VALUE SPACES.
             05 PL-PO-AMT          PIC Z,ZZZ,ZZZ.00-.
             05 FILLER             PIC X(01).
             05 PL-INV-AMT         PIC Z,ZZZ,ZZZ.00-.
             05 FILLER             PIC X(14).
             05 PL-PERCENT-PAID    PIC ZZZ.Z-.
             05 PL-PERCENT-SIGN    PIC X(01) VALUE '%'.
           03  FILLER              PIC X(80).

       01  WS-SAVED-PO-NBR         PIC X(08).
       01  WS-PRINT-CNT            PIC ZZZ,ZZZ.
       01  WS-ENC-LIFTED-CNT       PIC 9(06) VALUE 0.
       01  WS-BYPASS-CNT           PIC 9(06) VALUE 0.
       01  WS-CNCLD-PO-CNT         PIC 9(06) VALUE 0.
       01  WS-UNAPRVD-PO-CNT       PIC 9(06) VALUE 0.
       01  WS-TOTAL-PO-CNT         PIC 9(06) VALUE 0.
       01  WS-CHG-ORD-DONE-CNT     PIC 9(06) VALUE 0.
       01  WS-TOBE-PURGED-CNT      PIC 9(06) VALUE 0.
       01  WS-SELECTED-CNT         PIC 9(06) VALUE 0.
       01  WS-CHG-ORD-CNT          PIC 9(06) VALUE 0.
       01  WS-LESS-10-CNT          PIC 9(06) VALUE 0.
       01  WS-LESS-10USD-CNT       PIC 9(06) VALUE 0.
       01  WS-TERMINATED-CNT       PIC 9(06) VALUE 0.

           COPY BTCHWS.
           COPY CBCVDBXD.
           COPY FPUWK50.
           COPY FPUWD50.

           EXEC SQL
               INCLUDE CBCVDBCA
           END-EXEC.

           EXEC SQL
               INCLUDE SQLCA
           END-EXEC.

           EXEC SQL
               INCLUDE HVPOS
           END-EXEC.

           EXEC SQL
               INCLUDE HVPOA
           END-EXEC.

           EXEC SQL
               INCLUDE HVPOD
           END-EXEC.

           EXEC SQL
               INCLUDE HVIVD
           END-EXEC.

           EXEC SQL
               INCLUDE HVIVA
           END-EXEC.

           EXEC SQL
               INCLUDE HVIVH
           END-EXEC.

      *%****************************** *
      *%CURSOR FOR FORWARD SUBSET OF POD_PO_DTL_V
      *%****************************** *
           EXEC SQL
               DECLARE S_4096_4085_N CURSOR FOR
               SELECT
                  USER_CD
                 ,LAST_ACTVY_DT
                 ,UNVRS_CD
                 ,ITEM_NBR
                 ,FK_CDY_CMDTY_CD
                 ,CMDTY_DESC
                 ,UNIT_MEA_CD
                 ,ACTVY_DT
                 ,LIQDTN_IND
                 ,QTY
                 ,DTL_CNTR_ACCT
                 ,DTL_ERROR_IND
                 ,UNIT_PRICE
                 ,BLNKT_QTY_RMNG
                 ,TOTAL_QTY_RCVD
                 ,TOTAL_QTY_RJCTD
                 ,DLVR_TO_CD
                 ,TAX_IND
                 ,MODEL_NBR
                 ,MFR_NAME
                 ,TAX_AMT
                 ,PART_NBR
                 ,LIEN_CLOSED_CD
                 ,ITEM_DSCNT_AMT
                 ,DSCNT_BFR_TX_IND
                 ,PO_DSCNT_AMT
                 ,CNSLDTN_IND
                 ,PRICE_NEG_SIGN
                 ,GOV_OWND_IND
                 ,CDY_SETF
                 ,CDY_SET_TS
                 ,FK_POS_PO_NBR
                 ,FK_POS_CHG_SEQ_NBR
                 ,IDX_PO_DTL_TS
               FROM POD_PO_DTL_V
               WHERE FK_POS_PO_NBR = :POD-FK-POS-PO-NBR
                 AND FK_POS_CHG_SEQ_NBR = '   '
                 AND ITEM_NBR > :POD-ITEM-NBR
               ORDER BY ITEM_NBR  ASC
DEVBWS         FOR FETCH ONLY
               OPTIMIZE FOR 1 ROW
           END-EXEC.

       PROCEDURE DIVISION.
      /

      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   000-RUN-CONTROL ROUTINE                     *
      *%                                                               *
      *%---------------------------------------------------------------*
       000-RUN-CONTROL.

           PERFORM 010-INITIALIZE
              THRU 010-INITIALIZE-EXIT.
           PERFORM 020-MAINLINE
              THRU 020-MAINLINE-EXIT
                UNTIL WS-END-OF-PO   = 'Y'.
           PERFORM 030-WRAPUP
              THRU 030-WRAPUP-EXIT.

            GOBACK.

       000-RUN-CONTROL-EXIT.
           EXIT.

      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   010-INITIALIZE ROUTINE                      *
      *%                                                               *
      *%---------------------------------------------------------------*
       010-INITIALIZE.
           OPEN INPUT PO-FILE-IN
           OPEN OUTPUT PO-FILE-OT
           OPEN OUTPUT REPORTFILE
                                                                        13450000
           MOVE FUNCTION CURRENT-DATE(1:8) TO  WS-ACCEPT-DATE           13460000
           MOVE CORR WS-ACCEPT-DATE TO WS-ACCEPT-DATE2.                 13470000
           MOVE WS-ACCEPT-DATE2     TO H2-RUN-DATE.                     13480000
                                                                        13490000
           READ PO-FILE-IN INTO WS-PO-RECORD
                AT END MOVE 'Y' TO WS-END-OF-PO.

       010-INITIALIZE-EXIT.
           EXIT.


      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   020-MAINLINE ROUTINE                        *
      *%                                                               *
      *%---------------------------------------------------------------*
       020-MAINLINE.
           ADD 1 TO WS-TOTAL-PO-CNT
      *
           MOVE IN-ORDER-DT       TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO  ORDER-DATE-4096.

           IF   IN-DATE-TIME-TS    > SPACES
                 MOVE IN-DATE-TIME-TS TO
                                             DBLINKX-INPUT-PARM
                 MOVE '2' TO DBLINKX-REQUEST-CODE
                 MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
                 MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
                 CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
                 MOVE DBLINKX-OUTPUT-08 TO
                                            LAST-INV-PAID-DATE

                 MOVE IN-BLNKT-TERM-DT TO
                                            DBLINKX-INPUT-PARM
                 MOVE '2' TO DBLINKX-REQUEST-CODE
                 MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
                 MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
                 CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
                 MOVE DBLINKX-OUTPUT-08 TO
                                            LAST-INV-PAID-DATE
           ELSE
                MOVE ZERO TO LAST-INV-PAID-DATE.

           IF   IN-BLNKT-TERM-DT > SPACES
                 MOVE IN-BLNKT-TERM-DT TO
                                             DBLINKX-INPUT-PARM
                 MOVE '2' TO DBLINKX-REQUEST-CODE
                 MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
                 MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
                 CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
                 MOVE DBLINKX-OUTPUT-08 TO
                                            LAST-INV-PAID-DATE

                 MOVE IN-BLNKT-TERM-DT TO
                                            DBLINKX-INPUT-PARM
                 MOVE '2' TO DBLINKX-REQUEST-CODE
                 MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
                 MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
                 CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
                 MOVE DBLINKX-OUTPUT-08 TO
                                            BLNKT-TERM-DATE-4083
           ELSE
                MOVE ZERO TO BLNKT-TERM-DATE-4083.

            IF IN-PO-STATUS = 'N' OR 'C'
              IF  ORDER-DATE-4096  < +20060701
                  ADD 1 TO WS-CNCLD-PO-CNT
                  PERFORM 21-WRITE-TOBE-PURGED
                    THRU 21-EXIT
              END-IF
                GO TO 020-MAINLINE-EXIT
            END-IF.

           IF IN-PO-CLS  = 'B'
              IF (BLNKT-TERM-DATE-4083 > +20060701 ) OR
                 (LAST-INV-PAID-DATE   > +20070101)
                      ADD 1 TO WS-BYPASS-CNT
                       GO TO 020-MAINLINE-EXIT.

           PERFORM 27-CHECK-CHG-ORDER
              THRU 27-EXIT.
           IF DBLINK-MEMBER-COUNT > ZERO
                ADD 1 TO WS-CHG-ORD-DONE-CNT
                GO TO 020-MAINLINE-EXIT.

           IF ORDER-DATE-4096  > +20031231
               ADD 1 TO WS-BYPASS-CNT
               GO TO 020-MAINLINE-EXIT.

            PERFORM 21-WRITE-TOBE-PURGED
               THRU 21-EXIT.
       020-MAINLINE-EXIT.
           READ PO-FILE-IN INTO WS-PO-RECORD
                AT END MOVE 'Y' TO WS-END-OF-PO.

       21-WRITE-TOBE-PURGED.
             MOVE IN-PO-NBR           TO  HD-PO-NBR
                                          EXT-PO-NMBR
             MOVE IN-PO-CLS           TO  HD-PO-CLS
             MOVE IN-CHNG-SEQ-NBR TO      HD-CHNG-SEQ-NBR
             MOVE IN-PO-STATUS        TO  HD-PO-STATUS
             MOVE IN-ORDER-DT         TO  HD-ORDER-DT
             MOVE IN-BLNKT-TERM-DT TO HD-BLNKT-TERM-DT
             MOVE IN-PO-AMT           TO  HD-PO-AMT
             MOVE IN-INV-AMT          TO  HD-INV-AMT
             MOVE IN-ENC-AMT          TO  HD-ENC-AMT
             MOVE IN-PERCENT-PAID TO      HD-PERCENT-PAID
             MOVE IN-ENCY-LIFTED-YR TO HD-ENCY-LIFTED-YR
             MOVE IN-DATE-TIME-TS TO      HD-DATE-TIME-TS
             MOVE HD-PO-HEADER-RECORD TO DETAIL-LINE
              MOVE '01'           TO      EXT-UNVRS-CODE
              MOVE SPACES         TO      EXT-REC-TYPE


             IF DBLINK-MEMBER-COUNT > ZERO
                ADD 1 TO WS-CHG-ORD-DONE-CNT
             ELSE
                PERFORM 25-WRITE-REPORT
                   THRU 25-WRITE-REPORT-EXIT
                WRITE PO-RECORD-OT     FROM WS-PO-PURGE-EXTR
                ADD 1 TO WS-TOBE-PURGED-CNT
             END-IF.
       21-EXIT.
           EXIT.

       25-WRITE-REPORT.
                                                                        26150000
           IF  WS-LINE-CTR > 53                                         26200001
               PERFORM 26-HEADINGS                                      26210000
                  THRU 26-EXIT.                                         26220000

           WRITE REPORT02-RECORD FROM DETAIL-LINE AFTER 1.              26160000
                                                                        26170000
           ADD 1 TO WS-LINE-CTR.                                        26190000

       25-WRITE-REPORT-EXIT.
           EXIT.
      ***************************************************************** 22010000
       26-HEADINGS.                                                     22020000
      ***************************************************************** 22030000
                                                                        22040000
                                                                        22199600
           MOVE WS-PAGE TO H1-PAGE-NMBR.                                23060000
                                                                        23090000
           WRITE REPORT02-RECORD FROM HEADING-1 AFTER TOP-OF-PAGE.      23080000
           WRITE REPORT02-RECORD FROM HEADING-2 AFTER 1                 23080000
           WRITE REPORT02-RECORD FROM HEADING-3 AFTER 1                 23080000
           WRITE REPORT02-RECORD FROM HEADING-4 AFTER 1                 23080000
           WRITE REPORT02-RECORD FROM BLANK-LINE AFTER 1.               23080000
                                                                        23290000
           MOVE 5 TO WS-LINE-CTR.
           ADD  1 TO   WS-PAGE.                                         23060000
       26-EXIT.  EXIT.                                                  22020000
       27-CHECK-CHG-ORDER.
            IF IN-PO-STATUS = 'N' OR 'C'
               GO TO 27-EXIT.

            MOVE 0001 TO DBLINK-CURR-IO-NUM
            MOVE ZERO TO SQLCODE
                         DBLINK-MEMBER-COUNT
            MOVE IN-PO-NBR  TO POS-FK-POH-PO-NBR
            MOVE SPACES     TO POS-CHNG-SEQ-NBR

            IF IN-PO-CLS = 'L'
               CONTINUE
            ELSE
               MOVE IN-PO-NBR     TO      POS-FK-POH-PO-NBR
               MOVE IN-CHNG-SEQ-NBR TO    POS-CHNG-SEQ-NBR

               MOVE 0002 TO DBLINK-CURR-IO-NUM
               MOVE ZERO TO SQLCODE
               EXEC SQL
                    SELECT 1
                     INTO :DBLINK-MEMBER-COUNT
                    FROM POA_PO_ACCT_V
                    WHERE FK_POD_PO_NBR   = :POS-FK-POH-PO-NBR
                      AND FK_POD_CHG_SEQ_NBR = :POS-CHNG-SEQ-NBR
                      AND ACCT_CD IN ('637200', '638000')
                      AND RULE_CLS_CD= 'CCRD'
                   FETCH FIRST ROW ONLY
               END-EXEC
               IF SQLCODE = +100
                  MOVE ZERO TO DBLINK-MEMBER-COUNT.


       27-EXIT.
            EXIT.

      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   030-WRAPUP ROUTINE                          *
      *%                                                               *
      *%---------------------------------------------------------------*
       030-WRAPUP.
           CLOSE PO-FILE-IN
           CLOSE PO-FILE-OT
           CLOSE REPORTFILE.

           MOVE    WS-TOTAL-PO-CNT    TO WS-PRINT-CNT
           DISPLAY 'TOTAL PO READ              '    WS-PRINT-CNT

           DISPLAY ' '
           MOVE  WS-BYPASS-CNT                  TO WS-PRINT-CNT
           DISPLAY 'BYPASSED DUE TO DATE       ' WS-PRINT-CNT
           MOVE WS-CHG-ORD-DONE-CNT             TO WS-PRINT-CNT
           DISPLAY 'CHANGE ORDER DONE BYPASSED ' WS-PRINT-CNT
           DISPLAY ' '

           MOVE  WS-CNCLD-PO-CNT      TO WS-PRINT-CNT
           DISPLAY 'CNCLD OR UNAPPROVD -PURGED   ' WS-PRINT-CNT
           MOVE  WS-TOBE-PURGED-CNT              TO WS-PRINT-CNT
           DISPLAY 'TO BE PURGED COUNT         ' WS-PRINT-CNT.


       030-WRAPUP-EXIT.
           EXIT.


      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                  999-SQL-STATUS ROUTINE                       *
      *%                                                               *
      *%-------------------------------------------------------------- *
       999-SQL-STATUS.
           EXEC SQL
               INCLUDE BTCHERR
           END-EXEC.
       999-SQL-STATUS-EXIT.
           EXIT.

