       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDCCHG.
      ******************************************************************
      *    TITLE ......... REPLACE FDE.GRNT_INDR_COST
      *                    VALUES WITH DATA FROM INPUT FILE
      *    LANGUAGE ...... COBOL II / BATCH / DB2
      *    AUTHOR ........ M.JEWETT
      *    DATE-WRITTEN .. JUN 04, 2012
      *
      * 1. FUNCTION:
      *    THE PURPOSE OF THIS PROGRAM IS TO CHANGE THE GRANT INDIRECT
      *    COST FOR FY13 USING FILE "FISP.IDC.FY12".
      *
      *
      * 2. FILES:
      *    INPUT:   INP1 INPUT FILE        (SEQ)
      *             FDE_FUND_EFCTV_V       (DB2)
      *
      *    OUTPUT:  OUT1 OUTPUT FILE       (SEQ)
      *             FDE_FUND_EFCTV_V       (DB2)
      *
      ******************************************************************
      /
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT INP1-FILE            ASSIGN INP1.
           SELECT OUT1-FILE            ASSIGN OUT1.
           SELECT CRPT-FILE            ASSIGN CNTLRPT.

       DATA DIVISION.
       FILE SECTION.

       FD  INP1-FILE
           RECORDING MODE F
           BLOCK CONTAINS 0 RECORDS
           LABEL RECORDS STANDARD.
       01  INP1-RECORD                 PIC X(080).

       FD  OUT1-FILE
           RECORDING MODE F
           BLOCK CONTAINS 0 RECORDS
           LABEL RECORDS STANDARD.
       01  OUT1-RECORD                 PIC X(260).

       FD  CRPT-FILE
           RECORDING MODE F
           BLOCK CONTAINS 0 RECORDS
           LABEL RECORDS OMITTED.
       01  CRPT-RECORD                 PIC X(132).
      /
       WORKING-STORAGE SECTION.
      ******************************************************************
      *    MISCELLANEOUS WORK AREAS
      ******************************************************************
       01  FILLER                      PIC X(36)       VALUE
           'IDCCHG WORKING-STORAGE BEGINS HERE  '.

       01  WS-CONSTANTS.
           05  WS-THIS-PROGRAM         PIC X(08)       VALUE 'IDCCHG'.

       01  WS-ABEND-AREA.
           05  WS-ABEND-PARAGRAPH      PIC X(30)       VALUE SPACES.
           05  WS-ABEND-SQL-NBR        PIC S9(4)       VALUE ZEROES.
           05  WS-ABEND-CODE           PIC -999        VALUE ZEROES.

       01  WS-DATE-AREA.
           05  WS-SYS-DATE.
               10  WS-SYS-DATE-CC      PIC 9(02)       VALUE ZEROES.
               10  WS-SYS-DATE-YYMMDD.
                   15  WS-SYS-DATE-YY  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-MM  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-DD  PIC 9(02)       VALUE ZEROES.
           05  WS-SYSTEM-DATE          REDEFINES WS-SYS-DATE
                                       PIC 9(08).
           05  WS-RPT-DATE.
               10  WS-RPT-DATE-MM      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-DD      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-YY      PIC X(02)       VALUE SPACES.

       01  WS-MISC-AREA.
           05  WS-PREV-FUND-CD         PIC X(06)       VALUE LOW-VALUES.
           05  WS-NEW-START-DT         PIC X(10)   VALUE '2012-07-01'.
      /
      ******************************************************************
      *    INP1 INPUT RECORD AREA
      ******************************************************************
       01  INP1-COUNT                  PIC 9(08)       VALUE ZEROES.
       01  INP1-DUPL-COUNT             PIC 9(08)       VALUE ZEROES.
       01  INP1-AREA.
           05  INP1-FUND-CODE          PIC X(06)       VALUE SPACES.
           05  FILLER                  PIC X(06)       VALUE SPACES.
           05  INP1-NEW-IDC-CODE       PIC X(06)       VALUE SPACES.
           05  FILLER                  PIC X(62)       VALUE SPACES.

      ******************************************************************
      *    OUT1 OUTPUT RECORD AREA
      ******************************************************************
       01  OUT1-COUNT                  PIC 9(08)       VALUE ZEROES.
       01  OUT1-AREA.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  OUT1-FUND-CODE          PIC X(06)       VALUE SPACES.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  OUT1-STATUS             PIC X(01)       VALUE SPACES.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  OUT1-NEW-IDC-CODE       PIC X(06)       VALUE SPACES.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  OUT1-OLD-IDC-CODE       PIC X(06)       VALUE SPACES.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  OUT1-NEW-START-DT       PIC X(10)       VALUE SPACES.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  OUT1-OLD-START-DT       PIC X(10)       VALUE SPACES.
           05  FILLER                  PIC X(01)       VALUE SPACES.
           05  OUT1-MESSAGE            PIC X(20)       VALUE SPACES.
           05  FILLER                  PIC X(139)      VALUE SPACES.
      /
      ******************************************************************
      *    CONTROL REPORT AREA
      ******************************************************************
       01  DATE-CONV-COB2.             COPY WSDATEC2.

       01  TIME-CONV-COB2.             COPY WSTIMEC2.

       01  WHEN-COMP-CONV.             COPY WHENCMC2.

       01  CRPT-COUNTERS.
           05  CRPT-PAGE-COUNT         PIC 9(03)       VALUE ZEROES.
           05  CRPT-LINE-COUNT         PIC 9(03)       VALUE 99.

       01  CRPT-HEADING-1.
           05  FILLER                  PIC X(08)       VALUE 'IDCCHG'.
           05  FILLER                  PIC X(40)       VALUE SPACES.
           05  FILLER                  PIC X(35)       VALUE
               'UNIVERSITY OF CALIFORNIA, SAN DIEGO'.
           05  FILLER                  PIC X(35)       VALUE SPACES.
           05  CRPT-PAGE               PIC Z(04)       VALUE ZEROES.
           05  FILLER                  PIC X(10)       VALUE SPACES.

       01  CRPT-HEADING-2.
           05  FILLER                  PIC X(07)       VALUE 'RUN ON'.
           05  CRPT-RUN-DATE           PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(04)       VALUE ' AT'.
           05  CRPT-RUN-TIME.
               10  CRPT-RUN-TIME-HH    PIC 9(02)       VALUE ZEROES.
               10  FILLER              PIC X(01)       VALUE ':'.
               10  CRPT-RUN-TIME-MM    PIC 9(02)       VALUE ZEROES.
               10  FILLER              PIC X(01)       VALUE ':'.
               10  CRPT-RUN-TIME-SS    PIC 9(02)       VALUE ZEROES.
           05  FILLER                  PIC X(27)       VALUE SPACES.
           05  FILLER                  PIC X(78)       VALUE
               'CHANGE GRAND INDIRECT COST CODE FOR FY13 '.

       01  CRPT-HEADING-3.
           05  FILLER                  PIC X(59)       VALUE SPACES.
           05  FILLER                  PIC X(14)       VALUE
               'CONTROL REPORT'.
           05  FILLER                  PIC X(59)       VALUE SPACES.

       01  CRPT-DTL-LINE-1             PIC X(132)      VALUE SPACES.
      /
      ******************************************************************
      *    DB2 ERROR HANDLING WORK AREA
      ******************************************************************

           EXEC SQL INCLUDE DB2ERRBW
           END-EXEC.
      /
      ******************************************************************
      *    SQL AREA AND TABLE DB2 TABLE DCLGENS
      ******************************************************************

           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
       01  FDE-FUND-EFFC-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'FDE*'.
           05  FDE-SEL-COUNT           PIC 9(08)       VALUE ZEROES.
           05  FDE-NOTFND-COUNT        PIC 9(08)       VALUE ZEROES.
           05  FDE-NOCHNG-COUNT        PIC 9(08)       VALUE ZEROES.
           05  FDE-UPD-COUNT           PIC 9(08)       VALUE ZEROES.
           05  FDE-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVFDE
           END-EXEC.
      /
       PROCEDURE DIVISION.
      /
       A0000-MAINLINE.

      ******************************************************************
      *    PERFORM THE PROGRAM'S MAINLINE FUNCTIONS.
      ******************************************************************

           PERFORM A1000-INITIALIZATION.

           PERFORM B1000-MAIN-PROCESS.

           PERFORM A8000-FINALIZATION.

           GOBACK.
      /
       A1000-INITIALIZATION.
      ******************************************************************
      *    OPEN FILES AND INITIALIZE WORKING STORAGE AREAS.
      ******************************************************************

           MOVE 'A1000-INITIALIZATION' TO WS-ABEND-PARAGRAPH.

           DISPLAY '>>>>  IDCCHG STARTING  <<<<'.

           OPEN INPUT  INP1-FILE
                OUTPUT OUT1-FILE
                       CRPT-FILE.

           MOVE FUNCTION CURRENT-DATE(1:8)
                                       TO WS-SYS-DATE.
           MOVE WS-SYS-DATE-YY         TO WS-RPT-DATE-YY.
           MOVE WS-SYS-DATE-MM         TO WS-RPT-DATE-MM.
           MOVE WS-SYS-DATE-DD         TO WS-RPT-DATE-DD.
           MOVE WS-RPT-DATE            TO CRPT-RUN-DATE.

           COPY PDTIMEC2.

           MOVE TIME-HH                TO CRPT-RUN-TIME-HH.
           MOVE TIME-MM                TO CRPT-RUN-TIME-MM.
           MOVE TIME-SS                TO CRPT-RUN-TIME-SS.

           MOVE WHEN-COMPILED          TO COMPILED-DATA.
           INSPECT COMPILED-DATA-TIME
                   REPLACING ALL '.' BY ':'.
           STRING 'PROGRAM COMPILED '     COMPILED-DATA-DATE
                  ' AT '                  COMPILED-DATA-TIME
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.

           PERFORM A9000-PRINT-CRPT-DETAIL.
      /
       A8000-FINALIZATION.
      ******************************************************************
      *    REPORT/DISPLAY FINAL TOTALS AND CLOSE FILES.
      ******************************************************************

           MOVE 'A8000-FINALIZATION'   TO WS-ABEND-PARAGRAPH.

           COPY PDTIMEC2.

           MOVE 'PROGRAM STATUS'       TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF INPUT RECORDS READ        =>  '
                   INP1-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF DUPLICATE FUND CODES      =>  '
                   INP1-DUPL-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF FDE ROWS SELECTED         =>  '
                   FDE-SEL-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF FUND CODES NOT FOUND      =>  '
                   FDE-NOTFND-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF FDE ROWS UPDATED          =>  '
                   FDE-UPD-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF OUTPUT RECORDS WRITTEN    =>  '
                   OUT1-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'PROGRAM HAS BEEN SUCCESSFULLY COMPLETED AT '
                   TIME-HH ':' TIME-MM
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           MOVE ALL 'END OF REPORT '   TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           CLOSE INP1-FILE
                 OUT1-FILE
                 CRPT-FILE.

           DISPLAY '>>>>  IDCCHG FINISHED  <<<<'.
      /
       A9000-PRINT-CRPT-DETAIL.
      ******************************************************************
      *    WRITE CONTROL REPORT DETAIL LINE.
      ******************************************************************

           MOVE 'A9000-PRINT-CRPT-DETAIL' TO WS-ABEND-PARAGRAPH.

           IF CRPT-LINE-COUNT > 55
               PERFORM A9500-PRINT-CRPT-HEADING
           END-IF.

           WRITE CRPT-RECORD         FROM CRPT-DTL-LINE-1
                                    AFTER ADVANCING 2.

           MOVE SPACES                 TO CRPT-DTL-LINE-1.
           ADD  2                      TO CRPT-LINE-COUNT.


       A9500-PRINT-CRPT-HEADING.
      ******************************************************************
      *    WRITE CONTROL REPORT PAGE HEADINGS.
      ******************************************************************

           MOVE 'A9500-PRINT-CRPT-HEADING' TO WS-ABEND-PARAGRAPH.

           ADD  1                      TO CRPT-PAGE-COUNT.
           MOVE CRPT-PAGE-COUNT        TO CRPT-PAGE.

           WRITE CRPT-RECORD         FROM CRPT-HEADING-1
                                    AFTER ADVANCING PAGE.
           WRITE CRPT-RECORD         FROM CRPT-HEADING-2
                                    AFTER ADVANCING 1.
           WRITE CRPT-RECORD         FROM CRPT-HEADING-3
                                    AFTER ADVANCING 1.

           MOVE 3                      TO CRPT-LINE-COUNT.
      /
       B1000-MAIN-PROCESS.
      ******************************************************************
      *    READ THE INP1 INPUT FILE PARAMETER RECORD.
      *    PROCESS THE INP1 FILE UNTIL END OF FILE.
      ******************************************************************

           MOVE 'B1000-MAIN-PROCESS'   TO WS-ABEND-PARAGRAPH.

           PERFORM B1100-READ-INP1-FILE.

           PERFORM B2000-PROCESS-INP1 THRU B2000-EXIT
               UNTIL INP1-FUND-CODE = HIGH-VALUES.


       B1100-READ-INP1-FILE.
      ******************************************************************
      *    READ THE INP1 INPUT FILE.
      ******************************************************************

           MOVE 'B1100-READ-INP1-FILE' TO WS-ABEND-PARAGRAPH.

           READ INP1-FILE            INTO INP1-AREA
             AT END
               MOVE HIGH-VALUES        TO INP1-AREA
             NOT AT END
               ADD  1                  TO INP1-COUNT
           END-READ.

           INSPECT INP1-FUND-CODE
             CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
      /
       B2000-PROCESS-INP1.
      ******************************************************************
      *    PROCESS EACH INP1 RECORD TO UPDATE THE FDE TABLE.
      ******************************************************************

           MOVE 'B2000-PROCESS-INP1'   TO WS-ABEND-PARAGRAPH.

           MOVE SPACES                 TO OUT1-AREA.
           MOVE INP1-FUND-CODE         TO OUT1-FUND-CODE.

           MOVE INP1-NEW-IDC-CODE      TO OUT1-NEW-IDC-CODE.
           MOVE WS-NEW-START-DT        TO OUT1-NEW-START-DT.

           IF INP1-FUND-CODE = WS-PREV-FUND-CD
               ADD 1                   TO INP1-DUPL-COUNT
               MOVE 'DUPLICATE FUND CODE '
                                       TO OUT1-MESSAGE
               PERFORM B1100-READ-INP1-FILE
               GO TO B2000-EXIT
           ELSE
               MOVE INP1-FUND-CODE     TO WS-PREV-FUND-CD
           END-IF.

           MOVE INP1-FUND-CODE         TO FDE-FK2-FND-FUND-CD.
           PERFORM S2000-SELECT-FDE.

           IF FDE-SQLCODE = +0
               MOVE FDE-STATUS         TO OUT1-STATUS
               MOVE FDE-GRNT-INDRT-COST TO OUT1-OLD-IDC-CODE
               MOVE FDE-START-DT        TO OUT1-OLD-START-DT

               IF INP1-FUND-CODE       = FDE-FK2-FND-FUND-CD AND
                  INP1-NEW-IDC-CODE    = FDE-GRNT-INDRT-COST
                   ADD 1               TO FDE-NOCHNG-COUNT
                   MOVE 'NO CHANGE FOUND '
                                       TO OUT1-MESSAGE
               ELSE
                 MOVE INP1-NEW-IDC-CODE TO FDE-GRNT-INDRT-COST
                 MOVE 'IDCCHG'          TO FDE-USER-CD
                 MOVE WS-NEW-START-DT   TO FDE-START-DT
                 PERFORM S3000-INSERT-FDE
                 MOVE 'UPDATED'      TO OUT1-MESSAGE
               END-IF
           ELSE
               MOVE 'FUND CODE NOT FOUND '
                                       TO OUT1-MESSAGE
           END-IF.

           WRITE OUT1-RECORD         FROM OUT1-AREA.
           ADD  1                      TO OUT1-COUNT.

           PERFORM B1100-READ-INP1-FILE.

       B2000-EXIT.
           EXIT.
      /
       S2000-SELECT-FDE.
      ******************************************************************
      *    GET FDE_FUND_EFCTV_V INFORMATION.
      ******************************************************************

           MOVE 'S2000-SELECT-FDE'     TO WS-ABEND-PARAGRAPH.
           MOVE  +2000                 TO WS-ABEND-SQL-NBR.
MLJTST*
MLJTST*    EXEC SQL
MLJTST*        SELECT  FDE.FK2_FND_FUND_CD
MLJTST*               ,FDE.GRNT_INDRT_COST
MLJTST*          INTO :FDE-FK2-FND-FUND-CD
MLJTST*              ,:FDE-GRNT-INDRT-COST
MLJTST*          FROM  FDE_FUND_EFCTV_V         FDE
MLJTST*         WHERE  FDE.FK2_FND_FUND_CD      = :FDE-FK2-FND-FUND-CD
MLJTST*           AND  FDE.START_DT =
MLJTST*               (SELECT MAX(B.START_DT)
MLJTST*                  FROM FDE_FUND_EFCTV_V  B
MLJTST*                 WHERE B.FK2_FND_FUND_CD = :FDE-FK2-FND-FUND-CD
MLJTST*                   AND B.START_DT       <=  CURRENT_DATE)
MLJTST*           AND  FDE.TIME_STMP =
MLJTST*               (SELECT MAX(C.TIME_STMP)
MLJTST*                  FROM FDE_FUND_EFCTV_V  C
MLJTST*                 WHERE C.FK2_FND_FUND_CD = :FDE-FK2-FND-FUND-CD
MLJTST*                   AND C.START_DT        =  FDE.START_DT)
MLJTST*    END-EXEC.

           EXEC SQL
               SELECT
                   FDE.USER_CD
                  ,FDE.LAST_ACTVY_DT
                  ,FDE.START_DT
                  ,FDE.TIME_STMP
                  ,FDE.END_DT
                  ,FDE.STATUS
                  ,FDE.DATA_ENTRY_IND
                  ,FDE.FUND_CD
                  ,FDE.FUND_TITLE
                  ,FDE.PREDCSR_CD
                  ,FDE.FUND_TYP_CD
                  ,FDE.FDRL_FLW_THRGH_IND
                  ,FDE.RVNU_ACCT
                  ,FDE.ACRL_ACCT
                  ,FDE.CPTLZN_ACCT_CD
                  ,FDE.CPTLZN_FUND_CD
                  ,FDE.DFLT_ORGN_CD
                  ,FDE.DFLT_PRGRM_CD
                  ,FDE.DFLT_ACTVY_CD
                  ,FDE.DFLT_LCTN_CD
                  ,FDE.BANK_ACCT_CD
                  ,FDE.CUM_AUTH_AMT
                  ,FDE.GRNT_CNTRCT_NBR
                  ,FDE.PMS_CD
                  ,FDE.RPRT_CYCLE
                  ,FDE.BLLNG_FRMT
                  ,FDE.ATHRD_FNDNG_AMT
                  ,FDE.PAY_MTHD_CD
                  ,FDE.GRANT_COST_SHR
                  ,FDE.GRNT_COST_SHR_AMT
                  ,FDE.GRNT_INDRT_COST
                  ,FDE.ESTMD_CMPLN_DT
                  ,FDE.PRJCT_CLOSE_DT
                  ,FDE.CMPLT_IND
                  ,FDE.AGNCY_IREF_ID
                  ,FDE.MGR_IREF_ID
                  ,FDE.CNSTRCTN_IREF
                  ,FDE.INVGR_IREF_ID
                  ,FDE.CO_INVGR_IREF
                  ,FDE.FROM_BDGT_DAT
                  ,FDE.TO_BDGT_DT
                  ,FDE.FROM_GRANT_DT
                  ,FDE.TO_GRANT_DT
                  ,FDE.FROM_PRJCT_DT
                  ,FDE.TO_PRJCT_DT
                  ,FDE.ASSET_LCTN_CD
                  ,FDE.ROLL_BDGT_IND
                  ,FDE.AR_ACCT_ID_ONE
                  ,FDE.AR_ACCT_ID_NINE
                  ,FDE.FND_SETF
                  ,FDE.FK1_FND_FUND_CD
                  ,FDE.FND_SET_TS
                  ,FDE.FK2_FND_FUND_CD
                  ,FDE.FK3_FDT_FUND_TYPCD
               INTO
                   :FDE-USER-CD
                  ,:FDE-LAST-ACTVY-DT
                  ,:FDE-START-DT
                  ,:FDE-TIME-STMP
                  ,:FDE-END-DT
                  ,:FDE-STATUS
                  ,:FDE-DATA-ENTRY-IND
                  ,:FDE-FUND-CD
                  ,:FDE-FUND-TITLE
                  ,:FDE-PREDCSR-CD
                  ,:FDE-FUND-TYP-CD
                  ,:FDE-FDRL-FLW-THRGH-IND
                  ,:FDE-RVNU-ACCT
                  ,:FDE-ACRL-ACCT
                  ,:FDE-CPTLZN-ACCT-CD
                  ,:FDE-CPTLZN-FUND-CD
                  ,:FDE-DFLT-ORGN-CD
                  ,:FDE-DFLT-PRGRM-CD
                  ,:FDE-DFLT-ACTVY-CD
                  ,:FDE-DFLT-LCTN-CD
                  ,:FDE-BANK-ACCT-CD
                  ,:FDE-CUM-AUTH-AMT
                  ,:FDE-GRNT-CNTRCT-NBR
                  ,:FDE-PMS-CD
                  ,:FDE-RPRT-CYCLE
                  ,:FDE-BLLNG-FRMT
                  ,:FDE-ATHRD-FNDNG-AMT
                  ,:FDE-PAY-MTHD-CD
                  ,:FDE-GRANT-COST-SHR
                  ,:FDE-GRNT-COST-SHR-AMT
                  ,:FDE-GRNT-INDRT-COST
                  ,:FDE-ESTMD-CMPLN-DT
                  ,:FDE-PRJCT-CLOSE-DT
                  ,:FDE-CMPLT-IND
                  ,:FDE-AGNCY-IREF-ID
                  ,:FDE-MGR-IREF-ID
                  ,:FDE-CNSTRCTN-IREF
                  ,:FDE-INVGR-IREF-ID
                  ,:FDE-CO-INVGR-IREF
                  ,:FDE-FROM-BDGT-DAT
                  ,:FDE-TO-BDGT-DT
                  ,:FDE-FROM-GRANT-DT
                  ,:FDE-TO-GRANT-DT
                  ,:FDE-FROM-PRJCT-DT
                  ,:FDE-TO-PRJCT-DT
                  ,:FDE-ASSET-LCTN-CD
                  ,:FDE-ROLL-BDGT-IND
                  ,:FDE-AR-ACCT-ID-ONE
                  ,:FDE-AR-ACCT-ID-NINE
                  ,:FDE-FND-SETF
                  ,:FDE-FK1-FND-FUND-CD
                  ,:FDE-FND-SET-TS
                  ,:FDE-FK2-FND-FUND-CD
                  ,:FDE-FK3-FDT-FUND-TYPCD
                 FROM  FDE_FUND_EFCTV_V         FDE
                WHERE  FDE.FK2_FND_FUND_CD      = :FDE-FK2-FND-FUND-CD
                  AND  FDE.START_DT =
                      (SELECT MAX(B.START_DT)
                         FROM FDE_FUND_EFCTV_V  B
                        WHERE B.FK2_FND_FUND_CD = :FDE-FK2-FND-FUND-CD
                          AND B.START_DT       <=  CURRENT_DATE)
                  AND  FDE.TIME_STMP =
                      (SELECT MAX(C.TIME_STMP)
                         FROM FDE_FUND_EFCTV_V  C
                        WHERE C.FK2_FND_FUND_CD = :FDE-FK2-FND-FUND-CD
                          AND C.START_DT        =  FDE.START_DT)
           END-EXEC.

           MOVE SQLCODE                TO FDE-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO FDE-SEL-COUNT
             WHEN +100
               ADD 1                   TO FDE-NOTFND-COUNT
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S3000-INSERT-FDE.
      ******************************************************************
      *    INSERT FDE_FUND_EFCTV_V INFORMATION.
      ******************************************************************

           MOVE 'S3000-INSERT-FDE'     TO WS-ABEND-PARAGRAPH.
           MOVE  +3000                 TO WS-ABEND-SQL-NBR.

MLJTST*    EXEC SQL
      *        UPDATE  FDE_FUND_EFCTV_V
      *           SET  GRNT_INDRT_COST    = :FDE-GRNT-INDRT-COST
      *               ,USER_CD            = 'IDCCHG'
      *         WHERE  FK2_FND_FUND_CD    = :FDE-FK2-FND-FUND-CD
      *    END-EXEC.

           EXEC SQL
               INSERT INTO FDE_FUND_EFCTV_V
                 (USER_CD
                 ,LAST_ACTVY_DT
                 ,START_DT
                 ,TIME_STMP
                 ,END_DT
                 ,STATUS
                 ,DATA_ENTRY_IND
                 ,FUND_CD
                 ,FUND_TITLE
                 ,PREDCSR_CD
                 ,FUND_TYP_CD
                 ,FDRL_FLW_THRGH_IND
                 ,RVNU_ACCT
                 ,ACRL_ACCT
                 ,CPTLZN_ACCT_CD
                 ,CPTLZN_FUND_CD
                 ,DFLT_ORGN_CD
                 ,DFLT_PRGRM_CD
                 ,DFLT_ACTVY_CD
                 ,DFLT_LCTN_CD
                 ,BANK_ACCT_CD
                 ,CUM_AUTH_AMT
                 ,GRNT_CNTRCT_NBR
                 ,PMS_CD
                 ,RPRT_CYCLE
                 ,BLLNG_FRMT
                 ,ATHRD_FNDNG_AMT
                 ,PAY_MTHD_CD
                 ,GRANT_COST_SHR
                 ,GRNT_COST_SHR_AMT
                 ,GRNT_INDRT_COST
                 ,ESTMD_CMPLN_DT
                 ,PRJCT_CLOSE_DT
                 ,CMPLT_IND
                 ,AGNCY_IREF_ID
                 ,MGR_IREF_ID
                 ,CNSTRCTN_IREF
                 ,INVGR_IREF_ID
                 ,CO_INVGR_IREF
                 ,FROM_BDGT_DAT
                 ,TO_BDGT_DT
                 ,FROM_GRANT_DT
                 ,TO_GRANT_DT
                 ,FROM_PRJCT_DT
                 ,TO_PRJCT_DT
                 ,ASSET_LCTN_CD
                 ,ROLL_BDGT_IND
                 ,AR_ACCT_ID_ONE
                 ,AR_ACCT_ID_NINE
                 ,FND_SETF
                 ,FK1_FND_FUND_CD
                 ,FND_SET_TS
                 ,FK2_FND_FUND_CD
                 ,FK3_FDT_FUND_TYPCD
               )
               VALUES
                 (:FDE-USER-CD
                 ,:FDE-LAST-ACTVY-DT
                 ,:FDE-START-DT
                 ,:FDE-TIME-STMP
                 ,:FDE-END-DT
                 ,:FDE-STATUS
                 ,:FDE-DATA-ENTRY-IND
                 ,:FDE-FUND-CD
                 ,:FDE-FUND-TITLE
                 ,:FDE-PREDCSR-CD
                 ,:FDE-FUND-TYP-CD
                 ,:FDE-FDRL-FLW-THRGH-IND
                 ,:FDE-RVNU-ACCT
                 ,:FDE-ACRL-ACCT
                 ,:FDE-CPTLZN-ACCT-CD
                 ,:FDE-CPTLZN-FUND-CD
                 ,:FDE-DFLT-ORGN-CD
                 ,:FDE-DFLT-PRGRM-CD
                 ,:FDE-DFLT-ACTVY-CD
                 ,:FDE-DFLT-LCTN-CD
                 ,:FDE-BANK-ACCT-CD
                 ,:FDE-CUM-AUTH-AMT
                 ,:FDE-GRNT-CNTRCT-NBR
                 ,:FDE-PMS-CD
                 ,:FDE-RPRT-CYCLE
                 ,:FDE-BLLNG-FRMT
                 ,:FDE-ATHRD-FNDNG-AMT
                 ,:FDE-PAY-MTHD-CD
                 ,:FDE-GRANT-COST-SHR
                 ,:FDE-GRNT-COST-SHR-AMT
                 ,:FDE-GRNT-INDRT-COST
                 ,:FDE-ESTMD-CMPLN-DT
                 ,:FDE-PRJCT-CLOSE-DT
                 ,:FDE-CMPLT-IND
                 ,:FDE-AGNCY-IREF-ID
                 ,:FDE-MGR-IREF-ID
                 ,:FDE-CNSTRCTN-IREF
                 ,:FDE-INVGR-IREF-ID
                 ,:FDE-CO-INVGR-IREF
                 ,:FDE-FROM-BDGT-DAT
                 ,:FDE-TO-BDGT-DT
                 ,:FDE-FROM-GRANT-DT
                 ,:FDE-TO-GRANT-DT
                 ,:FDE-FROM-PRJCT-DT
                 ,:FDE-TO-PRJCT-DT
                 ,:FDE-ASSET-LCTN-CD
                 ,:FDE-ROLL-BDGT-IND
                 ,:FDE-AR-ACCT-ID-ONE
                 ,:FDE-AR-ACCT-ID-NINE
                 ,:FDE-FND-SETF
                 ,:FDE-FK1-FND-FUND-CD
                 ,:FDE-FND-SET-TS
                 ,:FDE-FK2-FND-FUND-CD
                 ,:FDE-FK3-FDT-FUND-TYPCD
               )
           END-EXEC.

           MOVE SQLCODE                TO FDE-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO FDE-UPD-COUNT
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S9999-DB2-ERROR.
      ******************************************************************
      *    DB2ERRBP - DB2-ERROR-ROUTINE   (DB2 ERROR HANDLING ROUTINE)
      *    DB2ERRBP DISPLAYS DB2 ERROR MSG TO SYSOUT, ROLLSBACK UPDATES,
      *    SETS RETURN-CODE TO '9', AND CALLS 'ABORT' TO PRODUCE A DUMP.
      ******************************************************************

           MOVE WS-THIS-PROGRAM        TO DB2-PROGRAM-NAME.
           MOVE WS-ABEND-SQL-NBR       TO DB2-DML-NUMBER.
           MOVE SQLCODE                TO WS-ABEND-CODE.

           DISPLAY 'ABNORMAL DB2 ERROR DETECTED IN PROGRAM '
                                          DB2-PROGRAM-NAME
           DISPLAY 'LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY 'PARAGRAPH / SQL NUMBER = '
                                          DB2-DML-NUMBER.
           DISPLAY 'DB2 SQLCODE = '       WS-ABEND-CODE.


           EXEC SQL INCLUDE DB2ERRBP
           END-EXEC.
      /
       Z9999-ABORT-PROGRAM.
      ******************************************************************
      *    AN ABNORMAL CONDITION HAS OCCURED.  ABORT PROGRAM PROCESSING.
      ******************************************************************

           DISPLAY ' '.
           DISPLAY '**************************************************'.
           DISPLAY '** ABNORMAL ERROR DETECTED IN PROGRAM '
                                          WS-THIS-PROGRAM.
           DISPLAY '** ERROR MESSAGE IS DISPLAYED BEFORE THIS TEXT BOX'.
           DISPLAY '** LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY '** PROGRAM PROCESSING IS BEING ABORTED'.
           DISPLAY '**************************************************'.

           MOVE '9' TO RETURN-CODE.
           CALL 'ABORT'.
