       IDENTIFICATION DIVISION.
       PROGRAM-ID.    ISRTZ9.
      ******************************************************************
      *    TITLE ......... INSERT Z9 ADDRESS FROM DEFAULT VENDOR.
      *    LANGUAGE ...... COBOL II / BATCH / DB2
      *    AUTHOR ........ M. RUSSELL
      *    DATE-WRITTEN .. JUNE, 2016
      *
      * 1. FUNCTION:
      *    THE PURPOSE OF THIS PROGRAM IS TO INSERT A Z9 ADDRESS ROW
      *    INTO THE ENA_ENTY_ADR_V OR PRA_PRSN_ADR_V COPIED FROM
      *    DEFAULT ADDRESS IN THE VDR_VENDOR_V TABLE.
      *
      * 2. FILES:
      *    INPUT:   VDR_VENDOR_V           (DB2)
      *
      *    OUTPUT:  ENA_ENTY_ADR_V         (DB2)
      *             PRA_PRSN_ADR_V         (DB2)
      *
      * 3. REPORTS: CRPT CONTROL REPORT
      *
      * 4. PROCESSING LOGIC:
      *
      *    PROCESS THE DB2 CURSOR TO RETRIEVE INFORMATION FROM THE
      *    VDR_VENDOR_V TABLE.
      *
      *    ORDER AND PROCESS ROWS IN THE FOLLOWING SEQUENCE GROUPS:
      *
      *    A) IF ENTPSN_IND = 'P'
      *          LOOKUP DEFAULT ADDRESS IN THE PRA_PRSN_ADR_V TABLE.
      *          COPY ROW TO A 'Z9' ADR_TYP_CD IN THE SAME TABLE.
      *
      *    B) IF ENTPSN_IND = 'E'
      *          LOOKUP DEFAULT ADDRESS IN THE ENA_ENTY_ADR_V TABLE.
      *          COPY ROW TO A 'Z9' ADR_TYP_CD IN THE SAME TABLE.
      *
      ******************************************************************

      ******************************************************************
      *                      MAINTENANCE HISTORY                       *
      ******************************************************************
      *    MODIFIED BY ..... DEVMR0                                    *
      *    DATE MODIFIED ... JUNE, 2016                                *
      *    MODIFICATION .... FP7010 - INSERT Z9 ADDRESSES.             *
      ******************************************************************
      /
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT CRPT-FILE            ASSIGN CNTLRPT.

       DATA DIVISION.
       FILE SECTION.

       FD  CRPT-FILE
           RECORDING MODE F
           BLOCK CONTAINS 0 RECORDS
           LABEL RECORDS OMITTED.
       01  CRPT-RECORD                 PIC X(132).
      /
       WORKING-STORAGE SECTION.
      ******************************************************************
      *    MISCELLANEOUS WORK AREAS
      ******************************************************************
       01  FILLER                      PIC X(36)       VALUE
           'ISRTZ9 WORKING-STORAGE BEGINS HERE'.

       01  WS-CONSTANTS.
           05  WS-THIS-PROGRAM         PIC X(08)       VALUE 'ISRTZ9'.

       01  WS-ABEND-AREA.
           05  WS-ABEND-PARAGRAPH      PIC X(30)       VALUE SPACES.
           05  WS-ABEND-SQL-NBR        PIC S9(4)       VALUE ZEROES.
           05  WS-ABEND-CODE           PIC -999        VALUE ZEROES.

       01  WS-DATE-AREA.
           05  WS-SYS-DATE.
               10  WS-SYS-DATE-CC      PIC 9(02)       VALUE ZEROES.
               10  WS-SYS-DATE-YYMMDD.
                   15  WS-SYS-DATE-YY  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-MM  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-DD  PIC 9(02)       VALUE ZEROES.
           05  WS-SYSTEM-DATE          REDEFINES WS-SYS-DATE
                                       PIC 9(08).
           05  WS-CMP-DATE.
               10  WS-CMP-DATE-MM      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-CMP-DATE-DD      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-CMP-DATE-CC      PIC X(02)       VALUE SPACES.
               10  WS-CMP-DATE-YY      PIC X(02)       VALUE SPACES.

           05  WS-RPT-DATE.
               10  WS-RPT-DATE-MM      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-DD      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-YY      PIC X(02)       VALUE SPACES.

       01  WS-WORK-AREA.
           05  WS-ADV-FK-EVT-EVNT-NBR  PIC X(08)       VALUE SPACES.
           05  WS-EVT-TOTL-ADVNC-AMT   PIC S9(9)V99    COMP-3
                                                       VALUE ZEROES.
           05  WS-TOT-ADVNC-AMT        PIC S9(9)V99    COMP-3
                                                       VALUE ZEROES.
           05  WS-MISMATCH-COUNT       PIC 9(03)       VALUE ZEROES.
           05  WS-COMPUTE-ADV-AMT      PIC S9(9)V99    COMP-3
                                                       VALUE ZEROES.
      /
      ******************************************************************
      *    CONTROL REPORT AREA
      ******************************************************************
       01  DATE-CONV-COB2.             COPY WSDATEC2.

       01  TIME-CONV-COB2.             COPY WSTIMEC2.

       01  WHEN-COMP-CONV.             COPY WHENCMC2.

       01  CRPT-COUNTERS.
           05  CRPT-PAGE-COUNT         PIC 9(03)       VALUE ZEROES.
           05  CRPT-LINE-COUNT         PIC 9(03)       VALUE 99.

       01  CRPT-HEADING-1.
           05  FILLER                  PIC X(08)       VALUE 'ISRTZ9'.
           05  FILLER                  PIC X(40)       VALUE SPACES.
           05  FILLER                  PIC X(35)       VALUE
               'UNIVERSITY OF CALIFORNIA, SAN DIEGO'.
           05  FILLER                  PIC X(35)       VALUE SPACES.
           05  CRPT-PAGE               PIC Z(04)       VALUE ZEROES.
           05  FILLER                  PIC X(10)       VALUE SPACES.

       01  CRPT-HEADING-2.
           05  FILLER                  PIC X(07)       VALUE 'RUN ON'.
           05  CRPT-RUN-DATE           PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(04)       VALUE ' AT'.
           05  CRPT-RUN-TIME.
               10  CRPT-RUN-TIME-HH    PIC 9(02)       VALUE ZEROES.
               10  FILLER              PIC X(01)       VALUE ':'.
               10  CRPT-RUN-TIME-MM    PIC 9(02)       VALUE ZEROES.
               10  FILLER              PIC X(01)       VALUE ':'.
               10  CRPT-RUN-TIME-SS    PIC 9(02)       VALUE ZEROES.
           05  FILLER                  PIC X(27)       VALUE SPACES.
           05  FILLER                  PIC X(78)       VALUE
               'BATCH REQUEST FILE LOAD '.

       01  CRPT-HEADING-3.
           05  FILLER                  PIC X(59)       VALUE SPACES.
           05  FILLER                  PIC X(14)       VALUE
               'CONTROL REPORT'.
           05  FILLER                  PIC X(59)       VALUE SPACES.

       01  CRPT-DTL-LINE-1             PIC X(132)      VALUE SPACES.
      /
      ******************************************************************
      *    DB2 ERROR HANDLING WORK AREA
      ******************************************************************

           EXEC SQL INCLUDE DB2ERRBW
           END-EXEC.
      /
      ******************************************************************
      *    SQL AREA AND TABLE DB2 TABLE DCLGENS
      ******************************************************************

           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
       01  VDR-VENDOR-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'VDR*'.
           05  VDR-COUNT               PIC 9(08)       VALUE ZEROES.
           05  VDR-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVVDR
           END-EXEC.
      /
       01  PRA-PRSN-ADDR-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'PRA*'.
           05  PRA-COUNT               PIC 9(08)       VALUE ZEROES.
           05  PRA-UPDATE-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRA-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPRA
           END-EXEC.
      /
       01  ENA-ENTY-ADDR-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'ENA*'.
           05  ENA-COUNT               PIC 9(08)       VALUE ZEROES.
           05  ENA-UPDATE-COUNT        PIC 9(08)       VALUE ZEROES.
           05  ENA-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVENA
           END-EXEC.
      /
       PROCEDURE DIVISION.
      /
       A0000-MAINLINE.
      ******************************************************************
      *    PERFORM THE PROGRAM'S MAINLINE FUNCTIONS.
      ******************************************************************

           PERFORM A1000-INITIALIZATION.

           PERFORM B1000-MAIN-PROCESS.

           PERFORM A8000-FINALIZATION.

           GOBACK.
      /
       A1000-INITIALIZATION.
      ******************************************************************
      *    OPEN FILES AND INITIALIZE WORKING STORAGE AREAS.
      ******************************************************************

           MOVE 'A1000-INITIALIZATION' TO WS-ABEND-PARAGRAPH.

           DISPLAY '>>>>  ISRTZ9 STARTING  <<<<'.

           OPEN OUTPUT CRPT-FILE.

           ADD 0 TO WS-MISMATCH-COUNT.

           MOVE FUNCTION CURRENT-DATE(1:8)
                                       TO WS-SYS-DATE.
           MOVE WS-SYS-DATE-YY         TO WS-RPT-DATE-YY.
           MOVE WS-SYS-DATE-MM         TO WS-RPT-DATE-MM.
           MOVE WS-SYS-DATE-DD         TO WS-RPT-DATE-DD.
           MOVE WS-RPT-DATE            TO CRPT-RUN-DATE.

           MOVE WS-SYS-DATE-CC         TO WS-CMP-DATE-CC.
           MOVE WS-SYS-DATE-YY         TO WS-CMP-DATE-YY.
           MOVE WS-SYS-DATE-MM         TO WS-CMP-DATE-MM.
           MOVE WS-SYS-DATE-DD         TO WS-CMP-DATE-DD.

           COPY PDTIMEC2.

           MOVE TIME-HH                TO CRPT-RUN-TIME-HH.
           MOVE TIME-MM                TO CRPT-RUN-TIME-MM.
           MOVE TIME-SS                TO CRPT-RUN-TIME-SS.

           MOVE WHEN-COMPILED          TO COMPILED-DATA.
           INSPECT COMPILED-DATA-TIME
                   REPLACING ALL '.' BY ':'.
           STRING 'PROGRAM COMPILED '     COMPILED-DATA-DATE
                  ' AT '                  COMPILED-DATA-TIME
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.

           PERFORM A9000-PRINT-CRPT-DETAIL.
      /
       A8000-FINALIZATION.
      ******************************************************************
      *    REPORT/DISPLAY FINAL TOTALS AND CLOSE FILES.
      ******************************************************************

           MOVE 'A8000-FINALIZATION'   TO WS-ABEND-PARAGRAPH.

           COPY PDTIMEC2.

           MOVE 'PROGRAM STATUS'       TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF VDR-CURSOR ROWS FETCHED   =>  '
                   VDR-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF PRA ROWS SELECTED         =>  '
                   PRA-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF PRA ROWS UPDATED          =>  '
                   PRA-UPDATE-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF ENA ROWS SELECTED         =>  '
                   ENA-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'NBR OF ENA ROWS UPDATED          =>  '
                   ENA-UPDATE-COUNT
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           STRING 'PROGRAM HAS BEEN SUCCESSFULLY COMPLETED AT '
                   TIME-HH ':' TIME-MM
                   DELIMITED BY SIZE INTO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           MOVE ALL 'END OF REPORT '   TO CRPT-DTL-LINE-1.
           PERFORM A9000-PRINT-CRPT-DETAIL.

           CLOSE CRPT-FILE.

           DISPLAY '>>>>  ISRTZ9 FINISHED  <<<<'.
      /
       A9000-PRINT-CRPT-DETAIL.
      ******************************************************************
      *    WRITE CONTROL REPORT DETAIL LINE.
      ******************************************************************

           MOVE 'A9000-PRINT-CRPT-DETAIL' TO WS-ABEND-PARAGRAPH.

           IF CRPT-LINE-COUNT > 55
               PERFORM A9500-PRINT-CRPT-HEADING
           END-IF.

           WRITE CRPT-RECORD         FROM CRPT-DTL-LINE-1
                                    AFTER ADVANCING 2.

           MOVE SPACES                 TO CRPT-DTL-LINE-1.
           ADD  2                      TO CRPT-LINE-COUNT.


       A9500-PRINT-CRPT-HEADING.
      ******************************************************************
      *    WRITE CONTROL REPORT PAGE HEADINGS.
      ******************************************************************

           MOVE 'A9500-PRINT-CRPT-HEADING' TO WS-ABEND-PARAGRAPH.

           ADD  1                      TO CRPT-PAGE-COUNT.
           MOVE CRPT-PAGE-COUNT        TO CRPT-PAGE.

           WRITE CRPT-RECORD         FROM CRPT-HEADING-1
                                    AFTER ADVANCING PAGE.
           WRITE CRPT-RECORD         FROM CRPT-HEADING-2
                                    AFTER ADVANCING 1.
           WRITE CRPT-RECORD         FROM CRPT-HEADING-3
                                    AFTER ADVANCING 1.

           MOVE 3                      TO CRPT-LINE-COUNT.
      /
       B1000-MAIN-PROCESS.
      ******************************************************************
      *    PROCESS THE VDR-CURSOR.
      ******************************************************************

           MOVE 'B1000-MAIN-PROCESS'   TO WS-ABEND-PARAGRAPH.

           PERFORM S1000-OPEN-VDR-CURSOR.

           PERFORM S1010-FETCH-VDR-CURSOR.

           PERFORM B2000-PROCESS-VDR-CURSOR
               UNTIL VDR-SQLCODE = +100.
MATTR ***      UNTIL PRA-UPDATE-COUNT  >  100.

           PERFORM S1020-CLOSE-VDR-CURSOR.
      /
       B2000-PROCESS-VDR-CURSOR.
      ******************************************************************
      *    FOR EACH TABLE VDR TABLE ROW:
      *    1) IF ENTPSN_IND = 'P'
      *          LOOKUP DEFAULT ADDRESS IN THE PRA_PRSN_ADR_V TABLE.
      *          COPY ROW TO A 'Z9' ADR_TYP_CD IN THE SAME TABLE.
      *
      *    2) IF ENTPSN_IND = 'E'
      *          LOOKUP DEFAULT ADDRESS IN THE ENA_ENTY_ADR_V TABLE.
      *          COPY ROW TO A 'Z9' ADR_TYP_CD IN THE SAME TABLE.
      ******************************************************************

           MOVE 'B2000-PROCESS-VDR-BREAK' TO WS-ABEND-PARAGRAPH.

           IF VDR-ENTPSN-IND = 'P'
              MOVE VDR-IREF-ID               TO PRA-FK-PRS-IREF-ID
              MOVE VDR-ADR-TYP-CD            TO PRA-ADR-TYP-CD
              PERFORM S2000-SELECT-PRA
              IF PRA-SQLCODE = ZERO
MATTR            DISPLAY 'INSERTING PRA: '
MATTR            DISPLAY 'PRA-FK-PRS-IREF-ID: ', PRA-FK-PRS-IREF-ID
                 MOVE 'Z9'                   TO PRA-ADR-TYP-CD
MATTR            DISPLAY 'PRA-ADR-TYP-CD    : ', PRA-ADR-TYP-CD
                 PERFORM S2050-INSERT-PRA-Z9
              END-IF
           ELSE
              MOVE VDR-VNDR-ID-LST-NINE      TO ENA-FK-ENT-ENTY-NINE
              MOVE VDR-ADR-TYP-CD            TO ENA-ADR-TYP-CD
              PERFORM S2100-SELECT-ENA
              IF ENA-SQLCODE = ZERO
MATTR            DISPLAY 'INSERTING ENA: '
MATTR            DISPLAY 'ENA-FK-ENT-ENTY-NINE: ', ENA-FK-ENT-ENTY-NINE
                 MOVE 'Z9'                   TO ENA-ADR-TYP-CD
MATTR            DISPLAY 'ENA-ADR-TYP-CD      : ', ENA-ADR-TYP-CD
                 PERFORM S2150-INSERT-ENA-Z9
              END-IF
           END-IF.

           PERFORM S1010-FETCH-VDR-CURSOR.

      /
       S1000-OPEN-VDR-CURSOR.
      ******************************************************************
      *    OPEN THE VDR-CURSOR FOR VDR_VENDOR_V.
      ******************************************************************

           MOVE 'S1000-OPEN-VDR-CURSOR'    TO WS-ABEND-PARAGRAPH.
           MOVE  +1000                     TO WS-ABEND-SQL-NBR.

           EXEC SQL
             DECLARE   VDR-CURSOR CURSOR FOR
               SELECT  VDR.USER_CD
                      ,VDR.LAST_ACTVY_DT
                      ,VDR.IREF_ID
                      ,VDR.ENTPSN_IND
                      ,VDR.VNDR_ID_LST_NINE
                      ,VDR.ADR_TYP_CD
                 FROM  VDR_VENDOR_V VDR
                 WHERE VDR.A1099_IND = 'Y'
                    OR VDR.FTB_592_IND = 'Y'
                ORDER BY
                       VDR.VNDR_ID_LST_NINE
           END-EXEC.

           EXEC SQL
             OPEN VDR-CURSOR
           END-EXEC.

           MOVE SQLCODE                TO VDR-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S1010-FETCH-VDR-CURSOR.
      ******************************************************************
      *    GET THE NEXT ROW FOR VDR-CURSOR.
      ******************************************************************

           MOVE 'S1010-FETCH-VDR-CURSOR'   TO WS-ABEND-PARAGRAPH.
           MOVE  +1010                     TO WS-ABEND-SQL-NBR.

           EXEC SQL
               FETCH   VDR-CURSOR
                INTO  :VDR-USER-CD
                     ,:VDR-LAST-ACTVY-DT
                     ,:VDR-IREF-ID
                     ,:VDR-ENTPSN-IND
                     ,:VDR-VNDR-ID-LST-NINE
                     ,:VDR-ADR-TYP-CD
           END-EXEC.

           MOVE SQLCODE                TO VDR-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO VDR-COUNT
MATTR *        DISPLAY 'VDR ROW RETRIEVED: '
MATTR *        DISPLAY 'VDR-VNDR-ID-LST-NINE: ', VDR-VNDR-ID-LST-NINE
MATTR *        DISPLAY 'VDR-ADR-TYP-CD      : ', VDR-ADR-TYP-CD
MATTR *        DISPLAY 'VDR-IREF-ID         : ', VDR-IREF-ID
MATTR *        DISPLAY 'VDR-ENTPSN-IND      : ', VDR-ENTPSN-IND
MATTR *        DISPLAY 'VDR-USER-CD         : ', VDR-USER-CD
MATTR *        DISPLAY 'VDR-LAST-ACTVY-DT   : ', VDR-LAST-ACTVY-DT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S1020-CLOSE-VDR-CURSOR.
      ******************************************************************
      *    CLOSE THE VDR-CURSOR.
      ******************************************************************

           MOVE 'S1020-CLOSE-VDR-CURSOR'   TO WS-ABEND-PARAGRAPH.
           MOVE  +1020                     TO WS-ABEND-SQL-NBR.

           EXEC SQL
               CLOSE   VDR-CURSOR
           END-EXEC.

           MOVE SQLCODE                TO VDR-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S2000-SELECT-PRA.
      ******************************************************************
      *    GET TABLE PRA.
      ******************************************************************

           MOVE 'S2000-SELECT-PRA'     TO WS-ABEND-PARAGRAPH.
           MOVE  +2000                 TO WS-ABEND-SQL-NBR.

           MOVE WS-CMP-DATE            TO PRA-END-DT.

           EXEC SQL
               SELECT   PRA.USER_CD
                       ,PRA.LAST_ACTVY_DT
                       ,PRA.ADR_TYP_CD
                       ,PRA.END_DT
                       ,PRA.START_DT
                       ,PRA.LINE_ADR_1
                       ,PRA.LINE_ADR_2
                       ,PRA.LINE_ADR_3
                       ,PRA.LINE_ADR_4
                       ,PRA.CITY_NAME
                       ,PRA.TLPHN_ID
                       ,PRA.CNTY_CD
                       ,PRA.STATE_CD
                       ,PRA.ZIP_CD
                       ,PRA.FK_ZIP_CNTRY_CD
                       ,PRA.LINE_ADR_1_UC
                       ,PRA.FAX_TLPHN_ID
                       ,PRA.EMADR_LINE
                       ,PRA.FAX_STOP
                       ,PRA.FK_PRS_IREF_ID
                       ,PRA.ZIP_SETF
                       ,PRA.FK_ZIP_ZIP_CD
                       ,PRA.ZIP_SET_TS
                       ,PRA.IDX_PRSN_ADR1_TS
                       ,PRA.COMMENT_LINE
                       ,PRA.PAYMT_METHOD_IND
                       ,PRA.LAST_UPDATE_TS
                 INTO  :PRA-USER-CD
                      ,:PRA-LAST-ACTVY-DT
                      ,:PRA-ADR-TYP-CD
                      ,:PRA-END-DT
                      ,:PRA-START-DT
                      ,:PRA-LINE-ADR-1
                      ,:PRA-LINE-ADR-2
                      ,:PRA-LINE-ADR-3
                      ,:PRA-LINE-ADR-4
                      ,:PRA-CITY-NAME
                      ,:PRA-TLPHN-ID
                      ,:PRA-CNTY-CD
                      ,:PRA-STATE-CD
                      ,:PRA-ZIP-CD
                      ,:PRA-FK-ZIP-CNTRY-CD
                      ,:PRA-LINE-ADR-1-UC
                      ,:PRA-FAX-TLPHN-ID
                      ,:PRA-EMADR-LINE
                      ,:PRA-FAX-STOP
                      ,:PRA-FK-PRS-IREF-ID
                      ,:PRA-ZIP-SETF
                      ,:PRA-FK-ZIP-ZIP-CD
                      ,:PRA-ZIP-SET-TS
                      ,:PRA-IDX-PRSN-ADR1-TS
                      ,:PRA-COMMENT-LINE
                      ,:PRA-PAYMT-METHOD-IND
                      ,:PRA-LAST-UPDATE-TS
                 FROM  PRA_PRSN_ADR_V PRA
                WHERE  PRA.FK_PRS_IREF_ID    = :PRA-FK-PRS-IREF-ID
                  AND  PRA.ADR_TYP_CD        = :PRA-ADR-TYP-CD
                  AND  PRA.END_DT            > :PRA-END-DT
           END-EXEC.

           MOVE SQLCODE                TO PRA-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO PRA-COUNT
MATTR *        DISPLAY 'PRA ROW RETRIEVED: '
MATTR *        DISPLAY 'PRA-FK-PRS-IREF-ID  : ', PRA-FK-PRS-IREF-ID
MATTR *        DISPLAY 'PRA-ADR-TYP-CD      : ', PRA-ADR-TYP-CD
MATTR *        DISPLAY 'PRA-LINE-ADR-1      : ', PRA-LINE-ADR-1
MATTR *        DISPLAY 'PRA-END-DT           : ', PRA-END-DT
MATTR *        DISPLAY 'PRA-IDX-PRSN-ADR1-TS : ', PRA-IDX-PRSN-ADR1-TS
             WHEN +100
MATTR          DISPLAY 'NO PRA ROW: ', PRA-FK-PRS-IREF-ID
             WHEN -811
MATTR          DISPLAY 'MULTIPLE ROWS FOUND FOR: ', PRA-FK-PRS-IREF-ID
MATTR          DISPLAY 'PRA-IDX-PRSN-ADR1-TS : ', PRA-IDX-PRSN-ADR1-TS
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S2050-INSERT-PRA-Z9.
      ******************************************************************
      *    INSERT TABLE PRA Z9 ENTRY.
      ******************************************************************

           MOVE 'S2050-INSERT-PRA'     TO WS-ABEND-PARAGRAPH.
           MOVE  +2050                 TO WS-ABEND-SQL-NBR.
MATTR      MOVE 'ISRTZ9'               TO PRA-USER-CD.

           EXEC SQL
               INSERT
                 INTO PRA_PRSN_ADR_V
                    ( USER_CD
                     ,LAST_ACTVY_DT
                     ,ADR_TYP_CD
                     ,END_DT
                     ,START_DT
                     ,LINE_ADR_1
                     ,LINE_ADR_2
                     ,LINE_ADR_3
                     ,LINE_ADR_4
                     ,CITY_NAME
                     ,TLPHN_ID
                     ,CNTY_CD
                     ,STATE_CD
                     ,ZIP_CD
                     ,FK_ZIP_CNTRY_CD
                     ,LINE_ADR_1_UC
                     ,FAX_TLPHN_ID
                     ,EMADR_LINE
                     ,FAX_STOP
                     ,FK_PRS_IREF_ID
                     ,ZIP_SETF
                     ,FK_ZIP_ZIP_CD
                     ,ZIP_SET_TS
      ***            ,IDX_PRSN_ADR1_TS
                     ,COMMENT_LINE
                     ,PAYMT_METHOD_IND
                     ,LAST_UPDATE_TS )
               VALUES
                     ( :PRA-USER-CD
                      ,:PRA-LAST-ACTVY-DT
                      ,:PRA-ADR-TYP-CD
                      ,:PRA-END-DT
                      ,:PRA-START-DT
                      ,:PRA-LINE-ADR-1
                      ,:PRA-LINE-ADR-2
                      ,:PRA-LINE-ADR-3
                      ,:PRA-LINE-ADR-4
                      ,:PRA-CITY-NAME
                      ,:PRA-TLPHN-ID
                      ,:PRA-CNTY-CD
                      ,:PRA-STATE-CD
                      ,:PRA-ZIP-CD
                      ,:PRA-FK-ZIP-CNTRY-CD
                      ,:PRA-LINE-ADR-1-UC
                      ,:PRA-FAX-TLPHN-ID
                      ,:PRA-EMADR-LINE
                      ,:PRA-FAX-STOP
                      ,:PRA-FK-PRS-IREF-ID
                      ,:PRA-ZIP-SETF
                      ,:PRA-FK-ZIP-ZIP-CD
                      ,:PRA-ZIP-SET-TS
      ***             ,:PRA-IDX-PRSN-ADR1-TS
                      ,:PRA-COMMENT-LINE
                      ,:PRA-PAYMT-METHOD-IND
                      ,:PRA-LAST-UPDATE-TS )
           END-EXEC.

           MOVE SQLCODE                TO PRA-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO PRA-UPDATE-COUNT
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S2100-SELECT-ENA.
      ******************************************************************
      *    GET TABLE ENA.
      ******************************************************************

           MOVE 'S2100-SELECT-ENA'     TO WS-ABEND-PARAGRAPH.
           MOVE  +2100                 TO WS-ABEND-SQL-NBR.

           MOVE WS-CMP-DATE            TO ENA-END-DT.

           EXEC SQL
               SELECT   ENA.USER_CD
                       ,ENA.LAST_ACTVY_DT
                       ,ENA.ADR_TYP_CD
                       ,ENA.END_DT
                       ,ENA.START_DT
                       ,ENA.LINE_ADR_1
                       ,ENA.LINE_ADR_2
                       ,ENA.LINE_ADR_3
                       ,ENA.LINE_ADR_4
                       ,ENA.CITY_NAME
                       ,ENA.TLPHN_ID
                       ,ENA.CNTY_CD
                       ,ENA.STATE_CD
                       ,ENA.ZIP_CD
                       ,ENA.FK_ZIP_CNTRY_CD
                       ,ENA.LINE_ADR_1_UC
                       ,ENA.FAX_TLPHN_ID
                       ,ENA.EMADR_LINE
                       ,ENA.FAX_STOP
                       ,ENA.ZIP_SETF
                       ,ENA.FK_ZIP_ZIP_CD
                       ,ENA.ZIP_SET_TS
                       ,ENA.FK_ENT_ENTY_ONE
                       ,ENA.FK_ENT_ENTY_NINE
                       ,ENA.IDX_ENTY_ADR1_TS
                       ,ENA.COMMENT_LINE
                       ,ENA.PAYMT_METHOD_IND
                       ,ENA.LAST_UPDATE_TS
                 INTO  :ENA-USER-CD
                      ,:ENA-LAST-ACTVY-DT
                      ,:ENA-ADR-TYP-CD
                      ,:ENA-END-DT
                      ,:ENA-START-DT
                      ,:ENA-LINE-ADR-1
                      ,:ENA-LINE-ADR-2
                      ,:ENA-LINE-ADR-3
                      ,:ENA-LINE-ADR-4
                      ,:ENA-CITY-NAME
                      ,:ENA-TLPHN-ID
                      ,:ENA-CNTY-CD
                      ,:ENA-STATE-CD
                      ,:ENA-ZIP-CD
                      ,:ENA-FK-ZIP-CNTRY-CD
                      ,:ENA-LINE-ADR-1-UC
                      ,:ENA-FAX-TLPHN-ID
                      ,:ENA-EMADR-LINE
                      ,:ENA-FAX-STOP
                      ,:ENA-ZIP-SETF
                      ,:ENA-FK-ZIP-ZIP-CD
                      ,:ENA-ZIP-SET-TS
                      ,:ENA-FK-ENT-ENTY-ONE
                      ,:ENA-FK-ENT-ENTY-NINE
                      ,:ENA-IDX-ENTY-ADR1-TS
                      ,:ENA-COMMENT-LINE
                      ,:ENA-PAYMT-METHOD-IND
                      ,:ENA-LAST-UPDATE-TS
                 FROM  ENA_ENTY_ADR_V ENA
                WHERE  ENA.FK_ENT_ENTY_NINE  = :ENA-FK-ENT-ENTY-NINE
                  AND  ENA.ADR_TYP_CD        = :ENA-ADR-TYP-CD
                  AND  ENA.END_DT            > :ENA-END-DT
           END-EXEC.

           MOVE SQLCODE                TO ENA-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO ENA-COUNT
MATTR *        DISPLAY 'ENA ROW RETRIEVED: '
MATTR *        DISPLAY 'ENA-FK-ENT-ENTY-NINE: ', ENA-FK-ENT-ENTY-NINE
MATTR *        DISPLAY 'ENA-ADR-TYP-CD      : ', ENA-ADR-TYP-CD
MATTR *        DISPLAY 'ENA-LINE-ADR-1      : ', ENA-LINE-ADR-1
MATTR *        DISPLAY 'ENA-END-DT           : ', ENA-END-DT
MATTR *        DISPLAY 'ENA-IDX-ENTY-ADR1-TS : ', ENA-IDX-ENTY-ADR1-TS
             WHEN +100
MATTR          DISPLAY 'NO ENA ROW: ', ENA-FK-ENT-ENTY-NINE
             WHEN -811
MATTR          DISPLAY 'MULTIPLE ROWS FOUND FOR: ', ENA-FK-ENT-ENTY-NINE
MATTR          DISPLAY 'ENA-FK-ENT-ENTY-NINE: ', ENA-FK-ENT-ENTY-NINE
MATTR          DISPLAY 'ENA-IDX-ENTY-ADR1-TS : ', ENA-IDX-ENTY-ADR1-TS
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S2150-INSERT-ENA-Z9.
      ******************************************************************
      *    INSERT TABLE ENA Z9 ENTRY.
      ******************************************************************

           MOVE 'S2150-INSERT-ENA'     TO WS-ABEND-PARAGRAPH.
           MOVE  +2150                 TO WS-ABEND-SQL-NBR.
MATTR      MOVE 'ISRTZ9'               TO ENA-USER-CD.

           EXEC SQL
               INSERT INTO ENA_ENTY_ADR_V
                      ( USER_CD
                       ,LAST_ACTVY_DT
                       ,ADR_TYP_CD
                       ,END_DT
                       ,START_DT
                       ,LINE_ADR_1
                       ,LINE_ADR_2
                       ,LINE_ADR_3
                       ,LINE_ADR_4
                       ,CITY_NAME
                       ,TLPHN_ID
                       ,CNTY_CD
                       ,STATE_CD
                       ,ZIP_CD
                       ,FK_ZIP_CNTRY_CD
                       ,LINE_ADR_1_UC
                       ,FAX_TLPHN_ID
                       ,EMADR_LINE
                       ,FAX_STOP
                       ,ZIP_SETF
                       ,FK_ZIP_ZIP_CD
                       ,ZIP_SET_TS
                       ,FK_ENT_ENTY_ONE
                       ,FK_ENT_ENTY_NINE
      ***              ,IDX_ENTY_ADR1_TS
                       ,COMMENT_LINE
                       ,PAYMT_METHOD_IND
                       ,LAST_UPDATE_TS )
                 VALUES
                     ( :ENA-USER-CD
                      ,:ENA-LAST-ACTVY-DT
                      ,:ENA-ADR-TYP-CD
                      ,:ENA-END-DT
                      ,:ENA-START-DT
                      ,:ENA-LINE-ADR-1
                      ,:ENA-LINE-ADR-2
                      ,:ENA-LINE-ADR-3
                      ,:ENA-LINE-ADR-4
                      ,:ENA-CITY-NAME
                      ,:ENA-TLPHN-ID
                      ,:ENA-CNTY-CD
                      ,:ENA-STATE-CD
                      ,:ENA-ZIP-CD
                      ,:ENA-FK-ZIP-CNTRY-CD
                      ,:ENA-LINE-ADR-1-UC
                      ,:ENA-FAX-TLPHN-ID
                      ,:ENA-EMADR-LINE
                      ,:ENA-FAX-STOP
                      ,:ENA-ZIP-SETF
                      ,:ENA-FK-ZIP-ZIP-CD
                      ,:ENA-ZIP-SET-TS
                      ,:ENA-FK-ENT-ENTY-ONE
                      ,:ENA-FK-ENT-ENTY-NINE
      ***             ,:ENA-IDX-ENTY-ADR1-TS
                      ,:ENA-COMMENT-LINE
                      ,:ENA-PAYMT-METHOD-IND
                      ,:ENA-LAST-UPDATE-TS )
           END-EXEC.

           MOVE SQLCODE                TO ENA-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO ENA-UPDATE-COUNT
             WHEN OTHER
               GO TO S9999-DB2-ERROR
           END-EVALUATE.
      /
       S9999-DB2-ERROR.
      ******************************************************************
      *    DB2ERRBP - DB2-ERROR-ROUTINE   (DB2 ERROR HANDLING ROUTINE)
      *    DB2ERRBP DISPLAYS DB2 ERROR MSG TO SYSOUT, ROLLSBACK UPDATES,
      *    SETS RETURN-CODE TO '9', AND CALLS 'ABORT' TO PRODUCE A DUMP.
      ******************************************************************

           MOVE WS-THIS-PROGRAM        TO DB2-PROGRAM-NAME.
           MOVE WS-ABEND-SQL-NBR       TO DB2-DML-NUMBER.
           MOVE SQLCODE                TO WS-ABEND-CODE.

           DISPLAY 'ABNORMAL DB2 ERROR DETECTED IN PROGRAM '
                                          DB2-PROGRAM-NAME
           DISPLAY 'LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY 'PARAGRAPH / SQL NUMBER = '
                                          DB2-DML-NUMBER.
           DISPLAY 'DB2 SQLCODE = '       WS-ABEND-CODE.


           EXEC SQL INCLUDE DB2ERRBP
           END-EXEC.
      /
       Z9999-ABORT-PROGRAM.
      ******************************************************************
      *    AN ABNORMAL CONDITION HAS OCCURED.  ABORT PROGRAM PROCESSING.
      ******************************************************************

           DISPLAY ' '.
           DISPLAY '**************************************************'.
           DISPLAY '** ABNORMAL ERROR DETECTED IN PROGRAM '
                                          WS-THIS-PROGRAM.
           DISPLAY '** ERROR MESSAGE IS DISPLAYED BEFORE THIS TEXT BOX'.
           DISPLAY '** LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY '** PROGRAM PROCESSING IS BEING ABORTED'.
           DISPLAY '**************************************************'.

           MOVE '9' TO RETURN-CODE.
           CALL 'ABORT'.
