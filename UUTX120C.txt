       IDENTIFICATION DIVISION.                                         00020000
       PROGRAM-ID. UUTX120C.
       AUTHOR.   ADCOM SYSTEMS                                          00040000
       DATE-WRITTEN.  2003-08-05.                                       00050000
       DATE-COMPILED.                                                   00060000
      *REMARKS.                                                         00070000
      ******************************************************************00080000
      *                                                                 00090000
      *    THIS PROGRAM WILL READ PAYROLL DATA AND RETRIEVE MOST CURRENT00100000
      *    ID AND XREF NUMBER                                           00110000
      *                                                                 00130000
      *                                                                 00180000
      *    INPUT:                                                       00140000
      *          ACD TABLE                                              00150000
      *                                                                 00160000
      *    OUTPUT:                                                      00170000
      *          CONTROLR      :  CONTROL REPORT                        00190000
      *          EXTRAC01      :  PAYROLL ACH TO CREATE                 00180000
      *                                                                 00200000
      *   MODIFICATIONS:                                                00202001
001MLJ* 02/01/08  MLJ  USE EMP-ID INSTEAD OF SSN                 45784  00203001
      ******************************************************************00210000
      *%-------------------------------------------------------------- *
      *%
      *%  *** TABLE VIEWS USED IN THIS PROGRAM
      *%
      *%  *** CURSORS DECLARED IN THIS PROGRAM
      *%
      *%
      *%  *** SUBROUTINES GENERATED FOR THIS PROGRAM
      *%
      *%
      *%-------------------------------------------------------------- *
       ENVIRONMENT DIVISION.                                            00250000
                                                                        00260000
       CONFIGURATION SECTION.                                           00270000
                                                                        00280000
       SPECIAL-NAMES.                                                   00320000
           C01 IS TOP-OF-PAGE.                                          00330000
                                                                        00340000
       INPUT-OUTPUT SECTION.                                            00320000
       FILE-CONTROL.                                                    00330000
                                                                        00340000
           COPY STDCRSL.
           SELECT EXTRAC01-FILE     ASSIGN TO EXTRAC01.                 00360000
                                                                        00370000
                                                                        00410000
       DATA DIVISION.                                                   00420000
                                                                        00460000
       FILE SECTION.                                                    00470000
                                                                        00370000
                                                                        00660000
       FD  EXTRAC01-FILE                                                00570000
           RECORDING MODE IS F                                          00570000
           LABEL RECORDS ARE OMITTED                                    00570000
           BLOCK CONTAINS 0 RECORDS                                     00570000
           RECORD CONTAINS 80  CHARACTERS                               00570000
           DATA RECORD IS EXTRAC01-RECORD.                              00570000
                                                                        00570000
       01  EXTRAC01-RECORD                  PIC X(80).                  00570000
                                                                        00660000
                                                                        00590000
       COPY STDCRFD.
                                                                        00590000
      /                                                                 00670000

       WORKING-STORAGE SECTION.                                         00680000
                                                                        00690000
      ****************************************************************  00720000
      *  PARM INPUT DETAILS                                          *  00730000
      ****************************************************************  00740000
       01  DBLINK-USER-CD   PIC X(8) VALUE 'UUTX120C'.
           COPY SYSRGHTC.
                                                                        00960000
      ****************************************************************  00890000
      *    CONSTANTS                                                 *  00900000
      ****************************************************************  00910000
       01  CONSTANTS.                                                   00920000
           03  CONST-PROG-NAME            PIC X(08)   VALUE 'UUTX120C'. 00930000
           03  CONST-PROG-START           PIC X(35)   VALUE             00940000
               '*** PROGRAM:  UUTX120C  STARTED ***'.                   00950000
           03  CONST-PROG-END             PIC X(35)   VALUE             00960000
               '*** PROGRAM:  UUTX120C  ENDED   ***'.                   00970000
                                                                        00980000
       01  WS-R6200-DBKEY                 PIC S9(8) COMP SYNC.
       01  FLAGS-AND-SWITCHES.                                          00990000
           03  WS-ACD-DONE                PIC X(01)   VALUE 'N'.        01060000
                                                                        01112000
       01  WS-COUNTERS.                                                 01113000
           03  WS-CNTL-RPT-PAGE-CNT      PIC 9(004) VALUE ZEROES.       01114000
           03  WS-CNTL-RPT-LINE-CNT      PIC 9(002) VALUE ZEROES.       01115000
           03  WS-TOTAL-ACD-READ         PIC 9(010) VALUE ZEROES.       01210000
           03  WS-TOTAL-PRS-REC          PIC 9(010) VALUE ZEROES.       01210000
           03  WS-TOTAL-ENT-REC          PIC 9(010) VALUE ZEROES.       01210000
           03  WS-ACH-ENDDATED           PIC 9(010) VALUE ZEROES.       01210000
           03  WS-STORE-EFCTV            PIC 9(010) VALUE ZEROES.       01210000
           03  WS-STORE-ACH              PIC 9(010) VALUE ZEROES.       01210000
           03  WS-STORE-VNDR             PIC 9(010) VALUE ZEROES.       01210000
           03  WS-STORE-TRVL             PIC 9(010) VALUE ZEROES.       01210000
           03  WS-WRITE-EXTR             PIC 9(010) VALUE ZEROES.       01210000
           03  WS-CTR-REC-READ           PIC Z(9)9 .                    01210000
                                                                        01213000
                                                                        01213000
       01  WS-DATES.                                                    01214000
           03  WS-SYSTEM-DATE             PIC 9(08) VALUE ZEROES.       01215000
           03  WS-PRENOTE-DATE            PIC 9(08) VALUE ZEROES.       01216000
           03  WS-END-DATE                PIC 9(08) VALUE ZEROES.       01216000
                                                                        02080000
       01  WS-ACD-RECORD.
001MLJ     03  ACD-EMP-ID                 PIC X(09).
           03  WS-NAME-KEY-6117           PIC X(35).
           03  FILLER                     PIC X(01).
           03  WS-ACD-ACH-DATA.
             05 WS-USER-CD                   PIC X(08).
             05 WS-LAST-ACTVY-DT             PIC X(05).
             05 WS-UNVRS-CD                  PIC X(02).
             05 WS-IREF-ID                   PIC S9(07).
             05 WS-ENTPSN-IND                PIC X(01).
             05 WS-ACH-ID-DGT-ONE            PIC X(01).
             05 WS-ACH-ID-LST-NINE           PIC X(09).
             05 WS-ACH-STOP-IND              PIC X(01).

       COPY STDWS.

       01  WS-COUNTERS.                                                 01113000
           03  WS-DETL-RPT-PAGE-CNT    PIC 9(004) VALUE ZEROES.         01114000
           03  WS-DETL-RPT-LINE-CNT    PIC 9(002) VALUE 66.             01115000
           03  WS-TOT-ERROR-LINES      PIC 9(002) VALUE ZEROES.         01118000
           03  WS-CNTL-REC-CNT         PIC 9(002) VALUE ZEROES.         01119000
           03  WS-CNTL-DTL-REC-CNT     PIC 9(002) VALUE ZEROES.         01120000
           03  WS-TOTAL-TOTAL-O        PIC X(006) VALUE SPACES.         01130000
           03  WS-TOTAL-AMT-O          PIC X(012) VALUE SPACES.         01130000
           03  WS-SUB                  PIC 9(03)  VALUE ZEROES.         01130000
           03  WS-SUBO                 PIC 9(03)  VALUE ZEROES.         01130000
      ****************************************************************  02360000
      *                    HOLD AREAS                                   02370000
      ****************************************************************  02380000
       01  HOLD-AREAS.                                                  02390000
           03  PRINT-RECORD.                                            02400000
               05 PRN-SKIPX                    PIC X(001) VALUE SPACES. 02410000
               05 PRN-LINE1                    PIC X(132) VALUE SPACES. 02420000
           03  SAVE-PRINT-RECORD.                                       02430000
               05  FILLER                      PIC X(001) VALUE SPACES. 02440000
               05  FILLER                      PIC X(132) VALUE SPACES. 02450000
           03  WS-HOLD-NAME                    PIC  X(55)  VALUE SPACES.
           03  WS-HOLD-NAME-DIGIT   REDEFINES  WS-HOLD-NAME
                                               PIC  X(01)  OCCURS 55.
           03  WS-HOLD-NAME-OUT                PIC  X(55)  VALUE SPACES.
           03  WS-HOLD-NAME-DIGITO  REDEFINES  WS-HOLD-NAME-OUT
                                               PIC  X(01)  OCCURS 55.
           03  WS-HOLD-PARMS.
               10  WS-HOLD-PARM-DATA           PIC  X(50)  VALUE SPACES.
               10  WS-HOLD-PARM-CONTROL        PIC  X(01)  VALUE SPACES.
           03  WS-ACH-LAST-NAME                PIC  X(35)  VALUE SPACES.
           03  WS-ACH-FIRST-NAME               PIC  X(35)  VALUE SPACES.
           03  WS-ACH-MIDDLE-INIT              PIC  X(01)  VALUE SPACES.
           03  WS-PAY-LAST-NAME                PIC  X(35)  VALUE SPACES.
           03  WS-PAY-FIRST-NAME               PIC  X(35)  VALUE SPACES.
           03  WS-PAY-MIDDLE-INIT              PIC  X(01)  VALUE SPACES.
      /                                                                 02520000
      ****************************************************************  02530000
      *  CONTROL REPORT                                                 02540000
      ***************************************************************** 02550000

       01  CONTROL-LINE-1.                                              02580000
           03  FILLER                  PIC X(048) VALUE                 02590000
               'PROGRAM NAME: UUTX120C '.                               02600000
           03  FILLER                  PIC X(065) VALUE                 02610000
               'UNIVERSITY OF CALIFORNIA, SAN DIEGO'.                   02620000
           03  FILLER                  PIC X(005) VALUE 'PAGE:'.        02630000
           03  CL1-PAGE-NUMBER         PIC ZZZ9.                        02640000
                                                                        02650000
       01  CONTROL-LINE-2.                                              02660000
           03  FILLER                  PIC X(051) VALUE                 02670000
               'REPORT:       REPORT01'.
           03  FILLER                  PIC X(045) VALUE                 02680000
               'IFIS ACH PAYROLL UPDATES               '.               02690000
           03  CL2-RETENTION           PIC X(031) VALUE SPACES.         02630000
                                                                        02700000
       01  CONTROL-LINE-3.                                              02710000
           03  FILLER                  PIC X(007) VALUE 'RUN ON'.       02720000
           03  CL3-RUN-DATE            PIC X(008).                      02730000
           03  FILLER                  PIC X(004) VALUE ' AT '.         02740000
           03  CL3-RUN-TIME-HH         PIC 9(002).                      02750000
           03  FILLER                  PIC X(001) VALUE ':'.            02760000
           03  CL3-RUN-TIME-MM         PIC 9(002).                      02770000
           03  FILLER                  PIC X(001) VALUE ':'.            02780000
           03  CL3-RUN-TIME-SS         PIC 9(002).                      02790000
           03  FILLER                  PIC X(032) VALUE SPACES.         02800000
           03  CL3-TITLE               PIC X(073) VALUE                 02810000
               'DETAIL    REPORT'.
                                                                        02830000
       01  CONTROL-LINE-4.                                              02710000
           03  FILLER                  PIC X(002) VALUE SPACES.         02720000
           03  FILLER                  PIC X(010) VALUE                 02750000
               ' ID NMBR  '.
           03  FILLER                  PIC X(002) VALUE SPACES.         02770000
           03  FILLER                  PIC X(035) VALUE                 02780000
               '        NAME          '.
           03  FILLER                  PIC X(002) VALUE SPACES.         02770000
           03  FILLER                  PIC X(044) VALUE                 02780000
               '              MODIFICATION                  '.
                                                                        02830000
       01  CONTROL-LINE-5.                                              02710000
           03  FILLER                  PIC X(002) VALUE SPACES.         02720000
           03  FILLER                  PIC X(010) VALUE                 02750000
               '----------'.
           03  FILLER                  PIC X(002) VALUE SPACES.         02770000
           03  FILLER                  PIC X(035) VALUE                 02780000
               '-----------------------------------'.
           03  FILLER                  PIC X(002) VALUE SPACES.         02770000
           03  FILLER                  PIC X(044) VALUE                 02780000
               '--------------------------------------------'.
                                                                        02830000
       01  CONTROL-LINE-6.                                              02710000
           03  FILLER                  PIC X(002) VALUE SPACES.         02720000
           03  CTL-ACH-ID              PIC X(10)  VALUE SPACES.         02750000
           03  FILLER                  PIC X(002) VALUE SPACES.         02770000
           03  CTL-ACH-NAME            PIC X(035) VALUE SPACES.         02780000
           03  FILLER                  PIC X(002) VALUE SPACES.         02800000
           03  CTL-ACH-REASON          PIC X(044) VALUE SPACES.         02780000
                                                                        02830000
       01  LINE-ADV-DETL-RPT-RECORD.                                    03190000
           03  DETAIL-REPORT-CC        PIC X(1) VALUE SPACE.            03200000
           03  DETAIL-REPORT-DATA      PIC X(132).                      03210000
                                                                        03220000
       01  HOLD-ADV-DETL-RPT-RECORD             PIC X(133).             02840000
      /                                                                 03230000
      *                                                                 03270000
           COPY BTCHWS.
       01  SUBSCHEMA-CONTROL  PIC X.
      *                                                                 03270000
      /                                                                 03310000

      *%****************************** *
      *% ACD_ACH_DATA_V DATA VIEW AREAS
      *%****************************** *
      *                                                                 03360000
           EXEC SQL
               INCLUDE HVACD
           END-EXEC.

      *%****************************** *
      *% PRS_PRSN_V DATA VIEW AREAS
      *%****************************** *
      *                                                                 03360000
           EXEC SQL
               INCLUDE HVPRS
           END-EXEC.

      *%****************************** *
      *% ENT_ENTY_V DATA VIEW AREAS
      *%****************************** *
      *                                                                 03360000
           EXEC SQL
               INCLUDE HVENT
           END-EXEC.
      *                                                                 03360000
           EXEC SQL
               INCLUDE CBCVDBCA
           END-EXEC.

      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                  CONTROL AREAS AND VIEWS                      *
      *%                                                               *
      *%-------------------------------------------------------------- *

      *%****************************** *
      *%CONVERT/DB COMMAREA
      *%****************************** *
           EXEC SQL
               INCLUDE SQLCA
           END-EXEC.

      *%****************************** *
      *%CURSOR FOR FORWARD SUBSET OF ACD_ACH_DATA_V
      *%****************************** *
           EXEC SQL
               DECLARE S_INDX_ACH_N CURSOR WITH HOLD FOR
               SELECT
                  USER_CD
                 ,LAST_ACTVY_DT
                 ,UNVRS_CD
                 ,IREF_ID
                 ,ENTPSN_IND
                 ,ACH_ID_DGT_ONE
                 ,ACH_ID_LST_NINE
                 ,ACH_STOP_IND
               FROM ACD_ACH_DATA_V
           END-EXEC.
      /

       PROCEDURE DIVISION.                                              03570000
      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%              000-SQL-INITIALIZATION ROUTINE                   *
      *%                                                               *
      *%-------------------------------------------------------------- *
                                                                        03610000
           PERFORM 1000-INITIALIZE                                      03620000
              THRU 1000-EXIT.
                                                                        03630000
           PERFORM 2000-MAINLINE                                        03620000
                 THRU 2000-EXIT
                 UNTIL WS-ACD-DONE = 'Y'.

           PERFORM 9000-END-OF-PROGRAM                                  03670000
              THRU 9000-EXIT.

       0000-EXIT.                                                       03590000
           EXIT.

      /                                                                 03680000
      ***************************************************************** 03690000
      *     INITIALIZATION ROUTINE                                    * 03700000
      ***************************************************************** 03710000
       1000-INITIALIZE.                                                 03720000
                                                                        03740000
           MOVE 'UUTX120C'           TO STD-WS-PROGRAM-NAME.
001MLJ     MOVE 'EMP-ID  MATCHING AGAINST PRS'
             TO STD-WS-CRPT-DESCR-1.
           PERFORM 99999-STD-CRPT-BEGIN.

           PERFORM 99999-STD-CRPT-INPUT-CNTRL-HDR.
           OPEN OUTPUT EXTRAC01-FILE
                                                                        04970000
           INITIALIZE WS-ACD-RECORD.

           MOVE ZERO TO SQLCODE
                        ERROR-STATUS

           MOVE 0010   TO DBLINK-CURR-IO-NUM.

           EXEC SQL
                OPEN S_INDX_ACH_N
           END-EXEC

           IF SQLCODE NOT = 0
               MOVE DBLINK-DB2-ERROR TO ERROR-STATUS
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           END-IF.

       1000-EXIT.
           EXIT.


      ****************************************************************  05330000
       2000-MAINLINE.
      ****************************************************************  05330000
            MOVE +0 TO SQLCODE
                       ERROR-STATUS

            MOVE +0015 TO DBLINK-CURR-IO-NUM

            EXEC SQL
                FETCH S_INDX_ACH_N
                INTO
                   :ACD-USER-CD
                  ,:ACD-LAST-ACTVY-DT
                  ,:ACD-UNVRS-CD
                  ,:ACD-IREF-ID
                  ,:ACD-ENTPSN-IND
                  ,:ACD-ACH-ID-DGT-ONE
                  ,:ACD-ACH-ID-LST-NINE
                  ,:ACD-ACH-STOP-IND
            END-EXEC.

           IF SQLCODE = +100
              MOVE 'Y' TO WS-ACD-DONE
              GO TO 2000-EXIT.

           ADD 1 TO WS-TOTAL-ACD-READ
           IF ACD-ENTPSN-IND   = 'P'
              ADD 1 TO WS-TOTAL-PRS-REC
001MLJ        PERFORM 2500-GET-PRSN-EMP-ID
                 THRU 2500-GET-PRSN-EMP-ID-EXIT
           ELSE
                 ADD 1 TO WS-TOTAL-ENT-REC
                 PERFORM 2600-GET-ENTY-FEIN
                    THRU 2600-GET-ENTY-FEIN-EXIT
           END-IF

001MLJ      IF SQLCODE = +0 AND ACD-EMP-ID > SPACES
                MOVE ACD-USER-CD         TO WS-USER-CD
                MOVE ACD-LAST-ACTVY-DT   TO WS-LAST-ACTVY-DT
                MOVE ACD-UNVRS-CD        TO WS-UNVRS-CD
                MOVE ACD-IREF-ID         TO WS-IREF-ID
                MOVE ACD-ENTPSN-IND      TO WS-ENTPSN-IND
                MOVE ACD-ACH-ID-DGT-ONE  TO WS-ACH-ID-DGT-ONE
                MOVE ACD-ACH-ID-LST-NINE TO WS-ACH-ID-LST-NINE
                MOVE ACD-ACH-STOP-IND    TO WS-ACH-STOP-IND

                WRITE EXTRAC01-RECORD FROM WS-ACD-RECORD
                ADD 1 TO WS-WRITE-EXTR
            ELSE
               IF SQLCODE = +100
                 DISPLAY 'NO PRS/ENT FOUND :'
                                     ACD-IREF-ID
                                     ' '
                                     ACD-ENTPSN-IND
            END-IF.

             INITIALIZE WS-ACD-RECORD.

       2000-EXIT.
            EXIT.

001MLJ 2500-GET-PRSN-EMP-ID.
            MOVE 2500 TO DBLINK-CURR-IO-NUM
            MOVE ACD-IREF-ID         TO PRS-IREF-ID

            PERFORM 906-05-R6117-PRSN
               THRU 906-05-R6117-PRSN-EXIT.

            IF SQLCODE NOT = +0 AND +100
               NEXT SENTENCE
            ELSE
               MOVE SQLCODE TO ERROR-STATUS
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT.

001MLJ      MOVE PRS-EMP-ID         TO ACD-EMP-ID.
            MOVE PRS-NAME-KEY       TO WS-NAME-KEY-6117.
001MLJ 2500-GET-PRSN-EMP-ID-EXIT.
            EXIT.

       2600-GET-ENTY-FEIN.
            MOVE 2600 TO DBLINK-CURR-IO-NUM
            MOVE ACD-IREF-ID         TO ENT-IREF-ID

            PERFORM 906-02-S-INDX-ENTY
               THRU 906-02-S-INDX-ENTY-EXIT.

            IF SQLCODE NOT = +0 AND +100
               NEXT SENTENCE
            ELSE
               MOVE SQLCODE TO ERROR-STATUS
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT.

001MLJ      MOVE ENT-FEIN-ID        TO ACD-EMP-ID.
            MOVE ENT-NAME-KEY       TO WS-NAME-KEY-6117.
       2600-GET-ENTY-FEIN-EXIT.
            EXIT.

      /                                                                 04420000
      ****************************************************************  05310000
      *  THE FOLLOWING PARAGRAPH WILL CLOSE EXTERNAL FILES              05320000
      ****************************************************************  05330000
       9000-END-OF-PROGRAM.

                                                                        24731001
           CLOSE EXTRAC01-FILE.

           EXEC SQL
               CLOSE S_INDX_ACH_N
           END-EXEC
           IF SQLCODE NOT = 0
               MOVE DBLINK-DB2-ERROR TO ERROR-STATUS
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           END-IF

           PERFORM 99999-STD-CRPT-WRITE-STATS.

           MOVE WS-TOTAL-ACD-READ         TO WS-CTR-REC-READ.
           STRING  'TOTAL ACD RECORDS READ      = '                     05590000
                   WS-CTR-REC-READ                                      05600000
               DELIMITED BY SIZE INTO STD-WS-CRPT-LINE.
           PERFORM 99999-STD-CRPT-WRITE-LINE.

           MOVE  WS-TOTAL-PRS-REC         TO WS-CTR-REC-READ.
           STRING  'TOTAL ACD PERSON  READ      = '                     05590000
                   WS-CTR-REC-READ                                      05600000
               DELIMITED BY SIZE INTO STD-WS-CRPT-LINE.
           PERFORM 99999-STD-CRPT-WRITE-LINE.

           MOVE  WS-TOTAL-ENT-REC         TO WS-CTR-REC-READ.
           STRING  'TOTAL ACD ENTITY  READ      = '                     05590000
                   WS-CTR-REC-READ                                      05600000
               DELIMITED BY SIZE INTO STD-WS-CRPT-LINE.
           PERFORM 99999-STD-CRPT-WRITE-LINE.

           MOVE WS-WRITE-EXTR             TO WS-CTR-REC-READ.
           STRING  'TOTAL ACD DATA REC WRITTEN  = '                     05590000
                   WS-CTR-REC-READ                                      05600000
               DELIMITED BY SIZE INTO STD-WS-CRPT-LINE.
           PERFORM 99999-STD-CRPT-WRITE-LINE.

           PERFORM 99999-STD-CRPT-FINISH.

           STOP RUN.

       9000-EXIT.
           EXIT.

       COPY STDCD.

      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                   900-START-SUBROUTINES                       *
      *%                                                               *
      *%-------------------------------------------------------------- *
       900-START-SUBROUTINES SECTION.
      *    CONVERT/DB SUBROUTINES FOLLOW
       900-START-SUBROUTINES-EXIT.
           EXIT.

      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                 906-05-R6117-PRSN ROUTINE                     *
      *%                                                               *
      *%-------------------------------------------------------------- *
       906-05-R6117-PRSN.
           MOVE '906-05-R6117-PRSN' TO DBLINK-CURR-PARAGRAPH.
           MOVE ZERO TO ERROR-STATUS.
           EXEC SQL
               SELECT
                  USER_CD
                 ,LAST_ACTVY_DT
                 ,FK_UNV_UNVRS_CD
                 ,IREF_ID
                 ,SSN_DGT_ONE
                 ,SSN_LST_NINE
                 ,NAME_KEY
                 ,NAME_SLTN_DESC
                 ,PRSN_FULL_NAME
                 ,NAME_SFX_DESC
                 ,BIRTH_DT
                 ,GNDR_FLAG
                 ,DCSD_FLAG
                 ,DCSD_DT
                 ,ALT_ID
                 ,PAN_NBR
                 ,PAN_FLAG
                 ,PAN_DT
                 ,MRTL_CD
                 ,RLGN_CD
                 ,ETHNC_CD
                 ,ARCHVD_DT
                 ,UNV_SETF
                 ,UNV_SET_TS
                 ,IDX_SSN_TS
001MLJ           ,EMP_ID
001MLJ           ,EMP_STATUS
               INTO
                  :PRS-USER-CD
                 ,:PRS-LAST-ACTVY-DT
                 ,:PRS-FK-UNV-UNVRS-CD
                 ,:PRS-IREF-ID
                 ,:PRS-SSN-DGT-ONE
                 ,:PRS-SSN-LST-NINE
                 ,:PRS-NAME-KEY
                 ,:PRS-NAME-SLTN-DESC
                 ,:PRS-PRSN-FULL-NAME
                 ,:PRS-NAME-SFX-DESC
                 ,:PRS-BIRTH-DT
                 ,:PRS-GNDR-FLAG
                 ,:PRS-DCSD-FLAG
                 ,:PRS-DCSD-DT
                 ,:PRS-ALT-ID
                 ,:PRS-PAN-NBR
                 ,:PRS-PAN-FLAG
                 ,:PRS-PAN-DT
                 ,:PRS-MRTL-CD
                 ,:PRS-RLGN-CD
                 ,:PRS-ETHNC-CD
                 ,:PRS-ARCHVD-DT
                 ,:PRS-UNV-SETF
                 ,:PRS-UNV-SET-TS
                 ,:PRS-IDX-SSN-TS
001MLJ           ,:PRS-EMP-ID
001MLJ           ,:PRS-EMP-STATUS
               FROM PRS_PRSN_V
               WHERE IREF_ID = :PRS-IREF-ID
           END-EXEC.
       906-05-R6117-PRSN-EXIT.
           EXIT.
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                906-02-S-INDX-ENTY ROUTINE                     *
      *%                                                               *
      *%-------------------------------------------------------------- *
       906-02-S-INDX-ENTY.
           MOVE '906-02-S-INDX-ENTY' TO DBLINK-CURR-PARAGRAPH.
           MOVE ZERO TO ERROR-STATUS.
           MOVE ' ' TO DBLINK-SET-NAME-WORK.
           EXEC SQL
               SELECT
                  USER_CD
                 ,LAST_ACTVY_DT
                 ,UNVRS_CD
                 ,ENTY_ID_DGT_ONE
                 ,ENTY_ID_LST_NINE
                 ,NAME_KEY
                 ,ENTY_FULL_NAME
                 ,IREF_ID
                 ,PAN_NBR
                 ,PAN_FLAG
                 ,PAN_DT
                 ,FEIN_ID
               INTO
                  :ENT-USER-CD
                 ,:ENT-LAST-ACTVY-DT
                 ,:ENT-UNVRS-CD
                 ,:ENT-ENTY-ID-DGT-ONE
                 ,:ENT-ENTY-ID-LST-NINE
                 ,:ENT-NAME-KEY
                 ,:ENT-ENTY-FULL-NAME
                 ,:ENT-IREF-ID
                 ,:ENT-PAN-NBR
                 ,:ENT-PAN-FLAG
                 ,:ENT-PAN-DT
                 ,:ENT-FEIN-ID
               FROM ENT_ENTY_V
               WHERE IREF_ID = :ENT-IREF-ID
           END-EXEC.
           IF SQLCODE NOT = 0 AND +100
              MOVE SQLCODE TO ERROR-STATUS
              PERFORM 999-SQL-STATUS
                 THRU 999-SQL-STATUS-EXIT.

       906-02-S-INDX-ENTY-EXIT.
           EXIT.
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                  999-SQL-STATUS ROUTINE                       *
      *%                                                               *
      *%-------------------------------------------------------------- *

       999-SQL-STATUS.
           EXEC SQL
               INCLUDE BTCHERR
           END-EXEC.
       999-SQL-STATUS-EXIT.
           EXIT.

