       IDENTIFICATION DIVISION.
       PROGRAM-ID.    UPEI101.
      ******************************************************************
      *    TITLE ......... CHECK FOR INVALID EMAILS IN IFIS
      *    LANGUAGE ...... COBOL II / BATCH / DB2
      *    AUTHOR ........ M MCGILL
      *    DATE-WRITTEN .. MARCH 2005
      *
      * 1. FUNCTION:
      *    THE PURPOSE OF THIS PROGRAM IS TO LOCATE INVALID EMAILS
      *    ON THE PRA AND ENA TABLES.
      ******************************************************************
      /
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       INPUT-OUTPUT SECTION.
       DATA DIVISION.
       FILE SECTION.
      /
       WORKING-STORAGE SECTION.
      ******************************************************************
      *    MISCELLANEOUS WORK AREAS
      ******************************************************************
       01  FILLER                      PIC X(36)       VALUE
           'UPEI101 WORKING-STORAGE BEGINS HERE '.

       01  WS-ATSIGN-COUNT           PIC S9(04) COMP.
       01  WS-SPCLCH-COUNT           PIC S9(04) COMP.
       01  WS-QUOTE-COUNT            PIC S9(04) COMP.
       01  WS-QUOTE         PIC X(01) VALUE QUOTE.
       01  WS-EMAIL-ADDRESS PIC X(30).
       01  WORKING-EMAIL-ADDRESS PIC X(30).
       01  WS-EMAIL-NAME    PIC X(30).
       01  WS-EMAIL-NAME-RJUST PIC X(30) JUSTIFIED RIGHT.
       01  WS-EMAIL-NAME1   PIC X(30).
       01  WS-EMAIL-NAME2   PIC X(30).
       01  WS-SPECIAL-CHAR-SW PIC X(01).
           88  SPECIAL-CHAR-FOUND VALUE 'Y'.

       01  WS-EMAIL-NAME3.
           05 WS-EMAIL-NAME-DIGIT OCCURS 30 TIMES PIC X(01).

       01  WS-SPC-CHAR-BREAKDOWN PIC X(01).
           88 WS-SPECIAL-CHARS  VALUES '=' '(' ')' '<' '>' '@'
                                       ',' ';' ':' '\' '"' '.'
                                       '{' '}' '[' ']'.
       01  WS-EMAIL-ORG.
           05  UCSD-EMAIL-ORG PIC X(08).
               88  UCSD-SDSC-ORG  VALUES 'UCSD.EDU'
                                         'ucsd.edu'
                                         'sdsc.edu'
                                         'SDSC.EDU'.

           05  FILLER         PIC X(22).

       01  WS-LEADING-SPACES  PIC S9(04) COMP.
       01  WS-ACCUM-EMAIL-LNG PIC S9(04) COMP.

       01  NAME-SUB                   PIC 9(3) VALUE ZERO.

       01  WS-PRS-IREF-ID             PIC S9(07) COMP-3.
       01  WS-ENT-LAST-NINE           PIC X(09).
       01  WS-CONSTANTS.
           05  WS-THIS-PROGRAM         PIC X(08)       VALUE 'UPEI101 '.
           05  WS-HIGH-DATE            PIC X(10)       VALUE
                                       '2999-12-31'.
           05  WS-HIGH-DATE-TS         PIC X(26)       VALUE
                                       '2999-12-31-23.59.59.999999'.
           05  WS-LOW-DATE-TS          PIC X(26)       VALUE
                                       '1800-01-01-00.00.00.000001'.

       01  WS-ABEND-AREA.
           05  WS-ABEND-PARAGRAPH      PIC X(30)       VALUE SPACES.
           05  WS-ABEND-SQL-NBR        PIC S9(4)       VALUE ZEROES.
           05  WS-ABEND-CODE           PIC -999        VALUE ZEROES.

       01  WS-DATE-AREA.
           05  WS-SYS-DATE.
               10  WS-SYS-DATE-CC      PIC 9(02)       VALUE ZEROES.
               10  WS-SYS-DATE-YYMMDD.
                   15  WS-SYS-DATE-YY  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-MM  PIC 9(02)       VALUE ZEROES.
                   15  WS-SYS-DATE-DD  PIC 9(02)       VALUE ZEROES.
           05  WS-SYSTEM-DATE          REDEFINES WS-SYS-DATE
                                       PIC 9(08).
           05  WS-RPT-DATE.
               10  WS-RPT-DATE-MM      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-DD      PIC X(02)       VALUE SPACES.
               10  FILLER              PIC X(01)       VALUE '/'.
               10  WS-RPT-DATE-YY      PIC X(02)       VALUE SPACES.

           05  WS-DAYS-SEPARATED       PIC S9(8) COMP.
           05  WS-DAYS-IN-TWO-YEARS    PIC S9(8) COMP VALUE +730.

           05  WS-CURRENT-TIMESTAMP    PIC X(26)       VALUE SPACES.
           05  FILLER REDEFINES WS-CURRENT-TIMESTAMP.
               10  WS-CURRENT-DATE     PIC X(10).
               10  FILLER              PIC X(01).
               10  WS-CURRENT-TIME     PIC X(08).
               10  FILLER              PIC X(01).
               10  WS-CURRENT-STAMP    PIC 9(06).

       01  UTEM100-LINK-PARM.
           05  UTEM100-LINK-EMAIL        PIC X(35).
           05  UTEM100-LINK-STATUS       PIC X(01).
               88 UTEM100-LINK-STATUS-OK  VALUE '0'.
               88 UTEM100-LINK-STATUS-BAD VALUE '1'.
           05  UTEM100-LINK-ERROR-MSG    PIC X(06).

       01  WS-WORK-AREA.
           05  WS-EMPID-COUNT          PIC S9(8) COMP  VALUE ZEROES.
           05  WS-SSN-IS-ZERO-COUNT    PIC 9(08)       VALUE ZEROES.
           05  WS-COMMIT-COUNT         PIC 9(08)       VALUE ZEROES.
           05  WS-PRA-ROW-READ         PIC 9(08)       VALUE ZEROES.
           05  WS-PRA-INV-EMAIL        PIC 9(08)       VALUE ZEROES.
           05  WS-ENA-ROW-READ         PIC 9(08)       VALUE ZEROES.
           05  WS-ENA-INV-EMAIL        PIC 9(08)       VALUE ZEROES.
           05  WS-PERSON-UPDATED-SW    PIC X(01)       VALUE 'N'.
           05  WS-ENTITY-EMP-SW        PIC X(01)       VALUE 'N'.
               88  EMPID-FOUND-AS-ENTITY               VALUE 'Y'.

           05  WS-MESSAGE.
               10  WS-MESSAGE-PRS      PIC X(16)       VALUE SPACES.
               10  WS-MESSAGE-SPLIT    PIC X(03)       VALUE SPACES.
               10  WS-MESSAGE-PRA      PIC X(16)       VALUE SPACES.
      /
      ******************************************************************
      *    DB2 ERROR HANDLING WORK AREA
      ******************************************************************

           EXEC SQL INCLUDE DB2ERRBW
           END-EXEC.
      /
      ******************************************************************
      *    SQL AREA AND TABLE DB2 TABLE DCLGENS
      ******************************************************************

           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
       01  PRA-PRSN-ADR-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'PRA*'.
           05  PRA-SELECT-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRA-BYPASS-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRA-UNCHNG-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRA-UPDATE-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRA-INSERT-COUNT        PIC 9(08)       VALUE ZEROES.
           05  PRA-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPRA
           END-EXEC.

      *%****************************** *
      *% PRA_PRSN_ADR_V DATA VIEW AREAS
      *%****************************** *
           COPY R6139.

       01  ENA-ENTY-ADR-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'ENA*'.
           05  ENA-SELECT-COUNT        PIC 9(08)       VALUE ZEROES.
           05  ENA-BYPASS-COUNT        PIC 9(08)       VALUE ZEROES.
           05  ENA-UNCHNG-COUNT        PIC 9(08)       VALUE ZEROES.
           05  ENA-UPDATE-COUNT        PIC 9(08)       VALUE ZEROES.
           05  ENA-INSERT-COUNT        PIC 9(08)       VALUE ZEROES.
           05  ENA-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVENA
           END-EXEC.

      *%****************************** *
      *% ENA_ENTY_ADR_V DATA VIEW AREAS
      *%****************************** *
           COPY R6185.

      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                    CURSOR DECLARATIONS                        *
      *%                                                               *
      *%-------------------------------------------------------------- *

      *%****************************** *
      *%CURSOR FOR FORWARD SUBSET OF ENA_ENTY_ADR_V
      *%****************************** *
           EXEC SQL
               DECLARE S_INDX_ENA_N CURSOR FOR
               SELECT
                  USER_CD
                 ,ADR_TYP_CD
                 ,LINE_ADR_1
                 ,LINE_ADR_1_UC
                 ,EMADR_LINE
                 ,FK_ENT_ENTY_ONE
                 ,FK_ENT_ENTY_NINE
               FROM ENA_ENTY_ADR_V
               WHERE FK_ENT_ENTY_ONE = ' '
                 AND FK_ENT_ENTY_NINE > :WS-ENT-LAST-NINE
               ORDER BY FK_ENT_ENTY_NINE ASC
               FOR FETCH ONLY
               OPTIMIZE FOR 1 ROW
           END-EXEC.

      *%****************************** *
      *%CURSOR FOR FORWARD SUBSET OF PRA_PRSN_ADR_V
      *%****************************** *
           EXEC SQL
               DECLARE S_INDX_PRA_N CURSOR FOR
               SELECT
                  USER_CD
                 ,ADR_TYP_CD
                 ,LINE_ADR_1
                 ,LINE_ADR_1_UC
                 ,EMADR_LINE
                 ,FK_PRS_IREF_ID
               FROM PRA_PRSN_ADR_V
               WHERE FK_PRS_IREF_ID > :WS-PRS-IREF-ID
               ORDER BY FK_PRS_IREF_ID ASC
               FOR FETCH ONLY
               OPTIMIZE FOR 1 ROW
           END-EXEC.
      /
       PROCEDURE DIVISION.
      /
       A0000-MAINLINE.

      ******************************************************************
      *    PERFORM THE PROGRAM'S MAINLINE FUNCTIONS.
      ******************************************************************

           PERFORM A1000-INITIALIZATION.

           DISPLAY '>>>>  UPEI101 PRA PROCESSING STARTING <<<<'.

           PERFORM B1000-PROCESS-PRA-ROWS THRU B1000-EXIT

           DISPLAY '>>>>  UPEI101 PRA PROCESSING COMPLETED <<<<'.
           DISPLAY ' ********************************************** '.
           DISPLAY ' PRA ROWS READ = ' WS-PRA-ROW-READ.
           DISPLAY ' PRA ROWS INVALID = ' WS-PRA-INV-EMAIL.
           DISPLAY ' ********************************************** '.

           DISPLAY '>>>>  UPEI101 ENA PROCESSING  STARTED <<<<'.

           PERFORM C1000-PROCESS-ENA-ROWS THRU C1000-EXIT

           DISPLAY '>>>>  UPEI101 ENA PROCESSING COMPLETED <<<<'.
           DISPLAY ' ********************************************** '.
           DISPLAY ' ENA ROWS READ = ' WS-ENA-ROW-READ.
           DISPLAY ' ENA ROWS INVALID = ' WS-ENA-INV-EMAIL.
           DISPLAY ' ********************************************** '.

           PERFORM A8000-FINALIZATION.

           GOBACK.
      /
       A1000-INITIALIZATION.
      ******************************************************************
      *    OPEN FILES AND INITIALIZE WORKING STORAGE AREAS.
      *    READ THE PARM INPUT FILE PARAMETER RECORD.
      ******************************************************************

           DISPLAY '>>>>  UPEI101 STARTING  <<<<'.
           MOVE ZEROES TO WS-PRA-ROW-READ
                          WS-PRA-INV-EMAIL
                          WS-ENA-ROW-READ
                          WS-ENA-INV-EMAIL.
      /
       A8000-FINALIZATION.
      ******************************************************************
      *    REPORT/DISPLAY FINAL TOTALS AND CLOSE FILES.
      ******************************************************************

           MOVE 'A8000-FINALIZATION'   TO WS-ABEND-PARAGRAPH.

           EXEC SQL
               CLOSE S_INDX_PRA_N
           END-EXEC

           EXEC SQL
               CLOSE S_INDX_ENA_N
           END-EXEC

           IF SQLCODE NOT = 0
               GO TO S9999-DB2-ERROR
           END-IF.

           DISPLAY '>>>>  UPEI101 FINISHED  <<<<'.
      /
       B1000-PROCESS-PRA-ROWS.

           MOVE 'B1000-PROCESS-PRA-ROWS' TO WS-ABEND-PARAGRAPH.

           MOVE ZEROES                 TO WS-PRS-IREF-ID.

           EXEC SQL
               OPEN S_INDX_PRA_N
           END-EXEC

           IF SQLCODE NOT = 0
               GO TO S9999-DB2-ERROR
           END-IF

           PERFORM B1500-FETCH-PRA-ROWS
              THRU B1500-EXIT
             UNTIL SQLCODE = +100.

       B1000-EXIT.
           EXIT.
      /
       B1500-FETCH-PRA-ROWS.

           MOVE 'B1500-FETCH-PRA-ROWS' TO WS-ABEND-PARAGRAPH.

           EXEC SQL
               FETCH S_INDX_PRA_N
              INTO :PRA-USER-CD
                  ,:PRA-ADR-TYP-CD
                  ,:PRA-LINE-ADR-1
                  ,:PRA-LINE-ADR-1-UC
                  ,:PRA-EMADR-LINE
                  ,:PRA-FK-PRS-IREF-ID
           END-EXEC.

           IF SQLCODE = +100
               GO TO B1500-EXIT
           END-IF.

           IF SQLCODE NOT = 0
               GO TO S9999-DB2-ERROR
           END-IF.

           MOVE PRA-FK-PRS-IREF-ID TO WS-PRS-IREF-ID.

           PERFORM B1750-CHECK-PRA-EMAIL
              THRU B1750-EXIT.

       B1500-EXIT.
           EXIT.
      /
       B1750-CHECK-PRA-EMAIL.

           MOVE 'B1750-CHECK-PRA-EMAIL' TO WS-ABEND-PARAGRAPH.

           IF PRA-ADR-TYP-CD NOT EQUAL ( 'ME' AND 'MG' AND 'ZZ' )
             GO TO B1750-EXIT
           END-IF.

           ADD 1 TO WS-PRA-ROW-READ.
           MOVE ZEROES TO WS-LEADING-SPACES
                          WS-ACCUM-EMAIL-LNG.

           IF PRA-ADR-TYP-CD = 'ZZ'
             MOVE PRA-EMADR-LINE
               TO UTEM100-LINK-EMAIL
           ELSE
             MOVE PRA-LINE-ADR-1
               TO UTEM100-LINK-EMAIL
           END-IF.

           CALL 'UTEM100'  USING UTEM100-LINK-PARM
           IF UTEM100-LINK-STATUS-BAD
             ADD 1 TO WS-PRA-INV-EMAIL
             DISPLAY ' IREF WITH INVALID EMAIL IS ' PRA-FK-PRS-IREF-ID  03520019
             DISPLAY ' INVALID EMAIL ADDRESS = ' UTEM100-LINK-EMAIL     03520019
           END-IF.

       B1750-EXIT.
           EXIT.
      /
       C1000-PROCESS-ENA-ROWS.

           MOVE 'C1000-PROCESS-ENA-ROWS' TO WS-ABEND-PARAGRAPH.

           MOVE LOW-VALUES             TO WS-ENT-LAST-NINE.

           EXEC SQL
               OPEN S_INDX_ENA_N
           END-EXEC

           IF SQLCODE NOT = 0
               GO TO S9999-DB2-ERROR
           END-IF

           PERFORM C1500-FETCH-ENA-ROWS
              THRU C1500-EXIT
             UNTIL SQLCODE = +100.

       C1000-EXIT.
           EXIT.
      /
       C1500-FETCH-ENA-ROWS.

           MOVE 'C1500-FETCH-ENA-ROWS' TO WS-ABEND-PARAGRAPH.

           EXEC SQL
               FETCH S_INDX_ENA_N
              INTO :ENA-USER-CD
                  ,:ENA-ADR-TYP-CD
                  ,:ENA-LINE-ADR-1
                  ,:ENA-LINE-ADR-1-UC
                  ,:ENA-EMADR-LINE
                  ,:ENA-FK-ENT-ENTY-ONE
                  ,:ENA-FK-ENT-ENTY-NINE
           END-EXEC.

           IF SQLCODE = +100
               GO TO C1500-EXIT
           END-IF.

           IF SQLCODE NOT = 0
               GO TO S9999-DB2-ERROR
           END-IF.

           MOVE ENA-FK-ENT-ENTY-NINE TO WS-ENT-LAST-NINE

           PERFORM C1750-CHECK-ENA-EMAIL
              THRU C1750-EXIT.

       C1500-EXIT.
           EXIT.
      /
       C1750-CHECK-ENA-EMAIL.

           MOVE 'C1750-CHECK-ENA-EMAIL' TO WS-ABEND-PARAGRAPH.

           IF ENA-ADR-TYP-CD NOT EQUAL ( 'ME' AND 'MG' )
             GO TO C1750-EXIT
           END-IF.

           ADD 1 TO WS-ENA-ROW-READ.
           MOVE ZEROES TO WS-LEADING-SPACES
                          WS-ACCUM-EMAIL-LNG.

           MOVE ENA-LINE-ADR-1
             TO UTEM100-LINK-EMAIL.

           CALL 'UTEM100'  USING UTEM100-LINK-PARM
           IF UTEM100-LINK-STATUS-BAD
             ADD 1 TO WS-ENA-INV-EMAIL
             DISPLAY ' ENA WITH INVALID EMAIL IS ' ENA-FK-ENT-ENTY-NINE 03520019
             DISPLAY ' INVALID EMAIL ADDRESS = ' UTEM100-LINK-EMAIL     03520019
           END-IF.

       C1750-EXIT.
           EXIT.
      /
       S9999-DB2-ERROR.
      ******************************************************************
      *    DB2ERRBP - DB2-ERROR-ROUTINE   (DB2 ERROR HANDLING ROUTINE)
      *    DB2ERRBP DISPLAYS DB2 ERROR MSG TO SYSOUT, ROLLSBACK UPDATES,
      *    SETS RETURN-CODE TO '9', AND CALLS 'ABORT' TO PRODUCE A DUMP.
      ******************************************************************

           MOVE WS-THIS-PROGRAM        TO DB2-PROGRAM-NAME.
           MOVE SQLCODE                TO WS-ABEND-CODE.

           DISPLAY 'ABNORMAL DB2 ERROR DETECTED IN PROGRAM '
                                          DB2-PROGRAM-NAME
           DISPLAY 'LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY 'PARAGRAPH / SQL NUMBER = '
           DISPLAY 'DB2 SQLCODE = '       WS-ABEND-CODE.

           DISPLAY 'ABEND OCCURRED FOR'
                  ' IREF# ' PRA-FK-PRS-IREF-ID.

           EXEC SQL INCLUDE DB2ERRBP
           END-EXEC.
      /
       Z9999-ABORT-PROGRAM.
      ******************************************************************
      *    AN ABNORMAL CONDITION HAS OCCURED.  ABORT PROGRAM PROCESSING.
      ******************************************************************

           DISPLAY ' '.
           DISPLAY '**************************************************'.
           DISPLAY '** ABNORMAL ERROR DETECTED IN PROGRAM '
                                          WS-THIS-PROGRAM.
           DISPLAY '** ERROR MESSAGE IS DISPLAYED BEFORE THIS TEXT BOX'.
           DISPLAY '** LAST PARAGRAPH ENTERED WAS '
                                          WS-ABEND-PARAGRAPH.
           DISPLAY '** PROGRAM PROCESSING IS BEING ABORTED'.
           DISPLAY '**************************************************'.

           MOVE '9' TO RETURN-CODE.
           CALL 'ABORT'.
